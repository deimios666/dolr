package ro.deimios.dolr.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.facade.FileFacadeLocal;
import ro.deimios.dolr.facade.FilecontentFacadeLocal;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.facade.TipTransferFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;
import ro.deimios.dolr.helper.CompressionHelper;
import ro.deimios.dolr.helper.CryptHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@RequestScoped
public class ResponseBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(ResponseBean.class.getName());

    @EJB
    private TipTransferFacadeLocal tipTransferFacade;
    @EJB
    private FileFacadeLocal fileFacade;
    @EJB
    private TransferFacadeLocal transferFacade;
    @EJB
    private FilecontentFacadeLocal filecontentFacade;
    @EJB
    private RegistruFacadeLocal registruFacade;

    private String fileHash;
    //private File file;
    private String fileName;

    /**
     * Creates a new instance of ResponseBean
     */
    public ResponseBean() {
    }

    /*public void saveFile(UploadedFile upFile) throws FileNotFoundException, IOException {
        fileHash = CryptHelper.sha512(upFile);
        String targetFileName = FacesContext.getCurrentInstance().getExternalContext().getRealPath("//WEB-INF//upload") + "//" + fileHash;
        FileHelper.saveUploadedFile(upFile.getInputstream(), targetFileName);
    }*/

    public void handleFileUpload(FileUploadEvent event) {
        try {
            //Uploaded file data
            LOG.fine("Getting inputstream");
            InputStream idCheckInputStream = event.getFile().getInputstream();
            InputStream uploadInputStream = event.getFile().getInputstream();
            LOG.fine("Creating workbook");
            Workbook workbook = new XSSFWorkbook(idCheckInputStream);
            LOG.fine("Getting ID sheet");
            Sheet idSheet = workbook.getSheet("ID");
            if (idSheet == null) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Macheta nu este destinată unității Dvs. Folosiți macheta descarcată de pe site!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }

            LOG.fine("Getting transfer ID");
            int transferId = (int) idSheet.getRow(1).getCell(0).getNumericCellValue();
            LOG.fine("Getting checksum");
            String checkSum = idSheet.getRow(0).getCell(0).getRichStringCellValue().getString();
            LOG.fine("Closing inputstream");
            idCheckInputStream.close();

            //Server data
            LOG.fine("Getting userbean");
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            LOG.fine("Getting sirues");
            long sirues = userBean.user.getUnitate().getSirues();
            LOG.fine("Getting transfer");
            Transfer transfer = transferFacade.find(transferId);
            if (transfer == null) {
                LOG.fine("Transfer not found");
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Transfer inexistent", "Această machetă nu mai este valabilă, descărcaț macheta modificată");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }
            LOG.fine("Getting transfer reciever sirues");
            long transferSirues = transfer.getReciever().getSirues();
            if (sirues != transferSirues) {
                LOG.fine("User sirues is not transfersirues");
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Macheta nu este destinată unității Dvs. Folosiți macheta descarcată de pe site!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }
            LOG.fine("Getting file hash");
            String dbFileHash = transfer.getAttachment().getHash();
            LOG.fine("Getting server hash");
            String serverHash = CryptHelper.sha512(dbFileHash + transferSirues);
            if (!serverHash.equals(checkSum)) {
                LOG.fine("Checksum is not equal server hash");
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Macheta nu este destinată unității Dvs. Folosiți macheta descarcată de pe site!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }
            LOG.fine("Getting filename");
            fileName = event.getFile().getFileName();
            LOG.fine("Saving file");
            //saveFile(event.getFile());
            fileHash = CryptHelper.sha512(event.getFile());
            LOG.fine("Find existing file");
            
            //save into DB
            ro.deimios.dolr.entity.Filecontent fileContentToDb = new ro.deimios.dolr.entity.Filecontent(null);
            fileContentToDb.setFilecontent(CompressionHelper.compress(uploadInputStream));
            
            LOG.fine("Persist file contents");
            filecontentFacade.create(fileContentToDb);
            
            ro.deimios.dolr.entity.File fileToDb = fileFacade.findByHash(fileHash);
            if (fileToDb == null) {
                LOG.fine("Existing file not found, creating");
                fileToDb = new ro.deimios.dolr.entity.File(null);
                fileToDb.setFilename(fileName);
                fileToDb.setHash(fileHash);
                fileToDb.setFilecontent(fileContentToDb);
                LOG.fine("Persist new file");
                fileFacade.create(fileToDb);
            }
            fileFacade.edit(fileToDb);
            fileContentToDb.setFile(fileToDb);
            filecontentFacade.edit(fileContentToDb);
            
            //set status
            LOG.fine("Checking termen");
            Date now = new Date();
            Date termen = transfer.getRegistru().getTermen();
            if (now.after(termen)) {
                LOG.fine("Termen Expired");
                transfer.setRaspStatus(2);
            } else {
                LOG.fine("Termen OK");
                transfer.setRaspStatus(1);
            }

            //create transfer
            LOG.fine("Creating new transfer");
            Transfer transferToDb = new Transfer(null);
            transferToDb.setAttachment(fileToDb);
            transferToDb.setSender(userBean.user.getUnitate());
            transferToDb.setReciever(transfer.getSender());
            transferToDb.setRaspunsLa(transfer);
            transferToDb.setSentOn(now);
            transferToDb.setTipTransfer(tipTransferFacade.find(2));
            transferToDb.setRegistru(transfer.getRegistru());
            LOG.fine("Persisting new transfer");
            transferFacade.create(transferToDb);

            LOG.fine("Modifying original transfer");
            if (transfer.getRaspCount() == null) {
                transfer.setRaspCount(0);
            }
            
            Registru registru = transfer.getRegistru();
            
            if (transfer.getRaspCount() == 0){
                registru.setNrresponses(registru.getNrresponses()+1);
            }
            
            registruFacade.edit(registru);

            transfer.setRaspCount(transfer.getRaspCount() + 1);
            transfer.setLastRaspuns(transferToDb);
            LOG.fine("Persisting original transfer");
            transferFacade.edit(transfer);
            
            if (transfer.getRaspStatus() == 2) {
                FacesMessage msgDupaTermen = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Termen Expirat", "Ați răspuns la solicitare după termenul stabilit, vă rugăm să respectați termenele în viitor!");
                FacesContext.getCurrentInstance().addMessage(null, msgDupaTermen);
            } else {
                FacesMessage msgInainteTermen = new FacesMessage(FacesMessage.SEVERITY_INFO, "Încadrare în termen", "Vă mulțumim că v-ați încadrat în termenul prestabilit.");
                FacesContext.getCurrentInstance().addMessage(null, msgInainteTermen);
            }
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Fișier Încărcat", "Fișierul '" + fileName + "' s-a încărcat cu succes ca răspuns la solicitarea: '" + transfer.getRegistru().getShortname() + "'. NR TRANSFER: " + transferToDb.getId());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul nu s-a încărcat");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (org.apache.poi.poifs.filesystem.OfficeXmlFileException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul încărcat este în format 2003 (xls)! Se acceptă doar format 2007 (xlsx)!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (org.apache.poi.hssf.OldExcelFormatException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul încărcat este în format excel 95! Se acceptă doar format 2007!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (NumberFormatException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Eroare Procesare", "Fișierul nu conține o machetă validă!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (NullPointerException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Folosiți macheta descarcată de pe site!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

    }
}
