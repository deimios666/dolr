package ro.deimios.dolr.bean;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.primefaces.event.FileUploadEvent;
import ro.deimios.dolr.entity.*;
import ro.deimios.dolr.facade.*;
import ro.deimios.dolr.helper.BeanHelper;
import ro.deimios.dolr.helper.CryptHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class SendBean implements Serializable {

    @EJB
    private TipTransferFacadeLocal tipTransferFacade;
    @EJB
    private TransferFacadeLocal transferFacade;
    @EJB
    private RegistruFacadeLocal registruFacade;
    @EJB
    private FileFacadeLocal fileFacade;
    @EJB
    private UnitateFacadeLocal unitateFacade;
    @EJB
    private FilecontentFacadeLocal filecontentFacade;
    private int fileContentId;
    private String titlu;
    private String responsabil;
    private Date publishDate;
    private Date termen;
    private List<Object[]> unitati;
    private List<ColumnModel> columns;
    private List<Object[]> selectedUnitati;
    private String fileHash;
    private String fileName;
    private TipStat tipStat;
    private static final Logger LOG = LogManager.getLogger(SendBean.class);

    /**
     * Creates a new instance of SendBean
     */
    public SendBean() {
        this.responsabil = " ";
    }

    @PostConstruct
    public void loadData() {
        unitati = unitateFacade.findAllNonISJPJWithAttributes();

        columns = new ArrayList<>();
        Object[] oneRow = unitati.get(0);

        for (int i = 0; i < oneRow.length; i++) {
            if (i > 0) {
                columns.add(new ColumnModel(i, ((UnitateAttrib) oneRow[i]).getAttribKey(), ((UnitateAttrib) oneRow[i]).getAttribKey()));
            }
        }

    }

    static public class ColumnModel implements Serializable {

        private final Integer id;
        private final String header;
        private final String property;

        public ColumnModel(Integer id, String header, String property) {
            this.id = id;
            this.header = header;
            this.property = property;
        }

        public Integer getId() {
            return id;
        }

        public String getHeader() {
            return header;
        }

        public String getProperty() {
            return property;
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            this.fileName = event.getFile().getFileName();
            this.fileHash = CryptHelper.sha512(event.getFile());
            //save
            ro.deimios.dolr.entity.Filecontent fileContentToDb = new ro.deimios.dolr.entity.Filecontent(null);

            byte[] compress = ro.deimios.dolr.helper.CompressionHelper.compress(event.getFile().getInputstream());
            LOG.info("Setting data");
            fileContentToDb.setFilecontent(compress);
            LOG.info("Creating new FileContent via facade");
            filecontentFacade.create(fileContentToDb);
            fileContentId = fileContentToDb.getId();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Fișier Încărcat", "Fișierul " + fileName + " s-a încărcat cu succes");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception ex) {
            LOG.info("Error uploadig file: "+ex);
            //System.out.println("Error uploadig file: "+ex);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul nu s-a încărcat");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void doSendStat() {
        
        //LOG.info("Encoding check: țȚșȘ / "+titlu );
        LOG.info("Starting sendstat");
        Unitate target;
        TipTransfer tipTransfer = tipTransferFacade.find(1); //set transfer type Adresă
        UserBean user = (UserBean) BeanHelper.getBean("userBean");

        //create registru
        Registru registru = new Registru(null);
        registru.setData(publishDate);
        registru.setShortname(titlu);
        registru.setResponsabil(responsabil);
        registru.setTermen(termen);
        registru.setTipStat(tipStat);
        registru.setNrresponses(0);
        registru.setNrtotal(selectedUnitati.size());
        registruFacade.create(registru);

        //save file
        ro.deimios.dolr.entity.File fileToDb = fileFacade.findByHash(fileHash);
        if (fileToDb == null) {
            fileToDb = new ro.deimios.dolr.entity.File(null);
            fileToDb.setFilename(fileName);
            fileToDb.setHash(fileHash);
            fileFacade.create(fileToDb);
        }

        Filecontent fileContentToDb = filecontentFacade.find(fileContentId);
        fileContentToDb.setFile(fileToDb);
        filecontentFacade.edit(fileContentToDb);
        fileToDb.setFilecontent(fileContentToDb);
        fileFacade.edit(fileToDb);

        //create transfers for each target
        Iterator<Object[]> targetWalker = selectedUnitati.iterator();
        while (targetWalker.hasNext()) {
            target = unitateFacade.find(((Unitate) targetWalker.next()[0]).getSirues());
            Transfer dbTransfer = new Transfer(null);
            dbTransfer.setTipTransfer(tipTransfer);
            dbTransfer.setSender(user.user.getUnitate());
            dbTransfer.setReciever(target);
            dbTransfer.setAttachment(fileToDb);
            dbTransfer.setRegistru(registru);
            dbTransfer.setSentOn(publishDate);
            dbTransfer.setRaspStatus(0);
            transferFacade.create(dbTransfer);
        }

        //Reset data
        titlu = "";
        termen = null;
        fileName = null;
        fileHash = null;

        //Reload lister data
        RegistruListerBean registruListerBean = (RegistruListerBean) BeanHelper.getBean("registruListerBean");
        registruListerBean.setTransferFacade(transferFacade);
        registruListerBean.loadData();

        //refresh the unitate regstru lister
        try {
            ((UnitateRegistruListerBean) BeanHelper.getBean("unitateRegistruListerBean")).loadData();
        } catch (Exception ignored) {
        }
    }

    public Date getTermen() {
        return termen;
    }

    public void setTermen(Date termen) {
        this.termen = termen;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getResponsabil() {
        return responsabil;
    }

    public void setResponsabil(String responsabil) {
        this.responsabil = responsabil;
    }
    
    public List<Object[]> getUnitati() {
        return unitati;
    }

    public void setUnitati(List<Object[]> unitati) {
        this.unitati = unitati;
    }

    public List<Object[]> getSelectedUnitati() {
        return selectedUnitati;
    }

    public void setSelectedUnitati(List<Object[]> selectedUnitati) {
        this.selectedUnitati = selectedUnitati;
    }

    public TipStat getTipStat() {
        return tipStat;
    }

    public void setTipStat(TipStat tipStat) {
        this.tipStat = tipStat;
    }

    public List<ColumnModel> getColumns() {
        return columns;
    }
    
    
    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

}
