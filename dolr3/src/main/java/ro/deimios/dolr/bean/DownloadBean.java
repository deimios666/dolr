package ro.deimios.dolr.bean;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetProtection;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.entity.TipStat;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.entity.Unitate;
import ro.deimios.dolr.entity.User;
import ro.deimios.dolr.facade.FileFacadeLocal;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.facade.UnitateFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;
import ro.deimios.dolr.helper.CompressionHelper;
import ro.deimios.dolr.helper.CryptHelper;
import ro.deimios.dolr.helper.ExcelHelper;
import ro.deimios.dolr.helper.FileHelper;
import ro.deimios.dolr.helper.ReportHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class DownloadBean {

    @EJB
    private RegistruFacadeLocal registruFacade;
    @EJB
    private TransferFacadeLocal transferFacade;
    @EJB
    private FileFacadeLocal fileFacade;
    @EJB
    private UnitateFacadeLocal unitateFacade;

    private static final Logger LOG = LogManager.getLogger();
    private static final int BUFFER_SIZE = 8096;

    private List<String> selectedUnitateAttributes;

    private int tid;
    private int rid;
    private String reportRange;
    private TipStat tipStat;

    /**
     * Creates a new instance of DownloadBean
     */
    public DownloadBean() {
    }

    public void downloadMacheta() {
        try {
            Transfer transfer = transferFacade.find(tid);

            String templateId = transfer.getAttachment().getHash();
            String fileName = FileHelper.sanitizeFileName(transfer.getRegistru().getShortname() + " (" + transfer.getRegistru().getResponsabil() + ")");
            fileName = "macheta_" + tid + fileName + "_";
            if (fileName.length() > 250) {
                fileName = fileName.substring(0, 250);
            }
            fileName = fileName + ".xlsx";
            fileName = URLEncoder.encode(fileName, "UTF-8");
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            User user = userBean.getUser();
            String unitateNume = user.getUnitate().getNume();
            String unitateSirues = "" + user.getUnitate().getSirues();
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
            ec.setResponseHeader("Content-Disposition", "attachment; filename*=UTF-8''" + fileName + ""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
            InputStream templateInputStream;
            Workbook template;
            XSSFSheet mainSheet;
            Sheet idSheet;
            String sheetPassword = CryptHelper.sha512(templateId + tid).substring(119); //get last 8 characters as password
            templateInputStream = new ByteArrayInputStream(CompressionHelper.decompress(transfer.getAttachment().getFilecontent().getFilecontent()));
            if (templateInputStream == null) {
                throw new FileNotFoundException("Template file not found!");
            }
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = (XSSFSheet) template.getSheetAt(0);

            CTSheetProtection sheetProtection = mainSheet.getCTWorksheet().getSheetProtection();
            boolean wasProtected = false;

            if (mainSheet.getProtect()) {
                //save protection and unprotect
                wasProtected = true;
                mainSheet.disableLocking();
            }

            TipStat tipStat = transfer.getRegistru().getTipStat();

            List<Unitate> arList;

            if (tipStat.getAr()) {
                arList = user.getUnitate().getUnitateList();
                Unitate firstFromList = arList.get(0);
                Long firstSirues = firstFromList.getSirues();
                String firstSiruesString = String.valueOf(firstSirues);
                template.setSheetName(0, firstSiruesString);
                //template.setSheetName(0, String.valueOf(arList.get(0).getSirues()));
                try {
                    String rawValue = mainSheet.getRow(0).getCell(0).getRawValue();
                } catch (NullPointerException ex) {
                    mainSheet.getRow(0).createCell(0).setCellValue(arList.get(0).getNume());
                }
                try {
                    mainSheet.getRow(0).getCell(1).setCellValue(arList.get(0).getNume());
                } catch (NullPointerException ex) {
                    mainSheet.getRow(0).createCell(1).setCellValue(arList.get(0).getNume());
                }

                for (int i = 0; i < arList.size(); i++) {
                    if (i > 0) {
                        Sheet newCopy = template.cloneSheet(0);
                        int copyIndex = template.getSheetIndex(newCopy);
                        String sirues = String.valueOf(arList.get(i).getSirues());
                        template.setSheetName(copyIndex, sirues);
                        template.setSheetOrder(sirues, i);
                        try {
                            newCopy.getRow(0).getCell(1).setCellValue(arList.get(i).getNume());
                        } catch (NullPointerException ex) {
                            newCopy.getRow(0).createCell(1).setCellValue(arList.get(i).getNume());
                        }
                        if (wasProtected) {
                            XSSFSheet xssfNewCopy = (XSSFSheet) newCopy;
                            xssfNewCopy.getCTWorksheet().setSheetProtection(sheetProtection);
                            xssfNewCopy.setSheetPassword(sheetPassword, null);
                            xssfNewCopy.enableLocking();
                        }
                    }
                }
            } else {
                try {
                    mainSheet.getRow(0).getCell(1).setCellValue(unitateNume);
                } catch (NullPointerException ex) {
                    mainSheet.getRow(0).createCell(1).setCellValue(unitateNume);
                }
            }
            if (wasProtected) {
                XSSFSheet xssfNewCopy = (XSSFSheet) mainSheet;
                xssfNewCopy.getCTWorksheet().setSheetProtection(sheetProtection);
                xssfNewCopy.setSheetPassword(sheetPassword, null);
                xssfNewCopy.enableLocking();
            }

            //recalcualte formulas
            //FormulaEvaluator evaluator = template.getCreationHelper().createFormulaEvaluator();
            for (int sheetNum = 0; sheetNum < template.getNumberOfSheets(); sheetNum++) {
                Sheet sheet = template.getSheetAt(sheetNum);
                sheet.setForceFormulaRecalculation(true);
                /*
                for (Row r : sheet) {
                    for (Cell c : r) {
                        if (c.getCellType() == Cell.CELL_TYPE_FORMULA) {
                            evaluator.evaluateFormulaCell(c);
                        }
                    }
                }*/
            }

            //get old ID sheet if it exists
            idSheet = template.getSheet("ID");
            if (idSheet == null) {
                idSheet = template.createSheet("ID");
            }
            template.setSheetOrder("ID", template.getNumberOfSheets() - 1);
            idSheet.createRow(0).createCell(0).setCellValue(CryptHelper.sha512(templateId + unitateSirues));
            idSheet.createRow(1).createCell(0).setCellValue(tid);
            //set sheet very hidden
            template.setSheetHidden(template.getNumberOfSheets() - 1, 2);
            OutputStream out = ec.getResponseOutputStream();
            template.write(out);
            out.flush();
            out.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //out.close();
        }

    }

    public void downloadReport() {
        try {
            int registruId = rid;
            int reportTypeId = 0;

            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            User user = userBean.getUser();
            //if no user set or unitate is not ISJ abort
            if (user == null || user.getUnitate().getTipUnitate().getId() != 4) {
                return;
            }
            Registru registru = registruFacade.find(registruId);
            boolean isVariable = registru.getTipStat().getVariable();
            //boolean isVariable = false;
            boolean is2D = registru.getTipStat().getD();
            boolean isAr = registru.getTipStat().getAr();

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"centralizator_" + registruId + ".xlsx\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
            List<Transfer> responseList = new ArrayList<>();
            List<Transfer> transferList = transferFacade.findAllByRegistruByTip(registruId, 1);

            for (int i = 0; i < transferList.size(); i++) {
                List<Transfer> raspList = transferFacade.findResponses(transferList.get(i).getId());
                //check for responses
                if (raspList == null || raspList.isEmpty()) {
                    //no responses, ignore
                } else {
                    responseList.add(raspList.get(raspList.size() - 1));
                }
            }
            Workbook workbook = ReportHelper.createReport(ec, !isVariable, reportRange, reportTypeId, isAr, responseList);

            try (OutputStream out = ec.getResponseOutputStream()) {
                workbook.write(out);
                out.flush();
                out.close();

            }
            fc.responseComplete();
        } catch (IOException ex) {
        }

    }

    public void downloadReportNg() {
        try {
            int registruId = rid;
            int reportTypeId = 0;

            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            User user = userBean.getUser();
            //if no user set or unitate is not ISJ abort
            if (user == null || user.getUnitate().getTipUnitate().getId() != 4) {
                return;
            }

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"centralizator_" + registruId + ".xlsx\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

            List<Object[]> data = transferFacade.getLastResponsesForRegistry(registruId, selectedUnitateAttributes);
            tipStat = registruFacade.find(registruId).getTipStat();

            Workbook workbook = ReportHelper.createReportNew(reportRange, data, tipStat, selectedUnitateAttributes, ec, fileFacade, unitateFacade);

            try (OutputStream out = ec.getResponseOutputStream()) {
                workbook.write(out);
                out.flush();
                out.close();

            }
            fc.responseComplete();
        } catch (IOException ex) {
        }

    }

    public void downloadOneFile() {
        try {
            Transfer transfer = transferFacade.find(tid);
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ro.deimios.dolr.entity.File file = transfer.getAttachment();
            String fileName = file.getFilename();

            int length;
            InputStream inputStream = new ByteArrayInputStream(CompressionHelper.decompress(transfer.getAttachment().getFilecontent().getFilecontent()));

            if (inputStream == null) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Descărcare", "Fișier inexistent: " + transfer.getAttachment().getHash());
                FacesContext.getCurrentInstance().addMessage(null, msg);
                throw new FileNotFoundException("File not found!");
            } else {
                ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
                if (fileName.endsWith("xlsx")) {
                    ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
                    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"file_" + tid + ".xlsx\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
                } else if (fileName.endsWith("xls")) {
                    ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
                    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"file_" + tid + ".xls\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
                }
                OutputStream outputStream = ec.getResponseOutputStream();
                byte[] buffer = new byte[BUFFER_SIZE];
                while (((length = inputStream.read(buffer)) != -1)) {
                    outputStream.write(buffer, 0, length);
                }
                outputStream.flush();
                outputStream.close();
            }
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    public void downloadZip() {
        try {

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/zip"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.

            int registruId = rid;

            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            User user = userBean.getUser();

            //if unitate is not ISJ abort
            if (user.getUnitate().getTipUnitate().getId() != 4) {
                return;
            }

            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"raspunsuri_" + registruId + ".zip\"");

            OutputStream outputStream = ec.getResponseOutputStream();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);
            byte buffer[] = new byte[BUFFER_SIZE];

            int transferId;
            String sheetPassword;
            String machetaHash;
            Workbook excelFile;

            //get transfers for the selected registru
            List<Transfer> transferList = transferFacade.findAllByRegistruByTip(registruId, 1);
            //get the hash for the file for the last response transfer
            for (int i = 0; i < transferList.size(); i++) {
                transferId = transferList.get(i).getId();
                machetaHash = transferList.get(i).getAttachment().getHash();
                sheetPassword = CryptHelper.sha512(machetaHash + transferId).substring(119); //get last 8 characters as password

                List<Transfer> raspList = transferFacade.findResponses(transferId);
                //check for responses
                if (raspList == null || raspList.isEmpty()) {
                    //no responses, ignore
                } else {
                    //get the latest transfer
                    Transfer raspuns = raspList.get(raspList.size() - 1);
                    //get the cleaned name of unitate
                    String unitateNume = raspuns.getSender().getNume().replaceAll("[\\\\/:\"*?<>|]+", "") + "_" + sheetPassword;
                    try {
                        InputStream fis = new ByteArrayInputStream(CompressionHelper.decompress(raspuns.getAttachment().getFilecontent().getFilecontent()));
                        excelFile = WorkbookFactory.create(fis);

                        Iterator<Sheet> sheetWalker = excelFile.sheetIterator();

                        while (sheetWalker.hasNext()) {
                            XSSFSheet currentSheet = (XSSFSheet) sheetWalker.next();
                            if (currentSheet.getProtect()) {
                                currentSheet.disableLocking();
                            }
                        }

                        ByteArrayOutputStream bos;
                        bos = new ByteArrayOutputStream();
                        excelFile.write(bos);
                        bos.flush();
                        bos.close();
                        excelFile.close();
                        
                        BufferedInputStream bis;
                        bis = new BufferedInputStream(new ByteArrayInputStream(bos.toByteArray()));

                        if (bis.available() >= 0) {

                            String fileExtension = ExcelHelper.getExt(raspuns.getAttachment().getFilename());
                            zos.putNextEntry(new ZipEntry(unitateNume + "." + fileExtension));

                            int bytesRead;
                            while ((bytesRead = bis.read(buffer)) != -1) {
                                zos.write(buffer, 0, bytesRead);
                            }
                            zos.closeEntry();
                        }
                    } catch (IOException | EncryptedDocumentException | InvalidFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            zos.flush();
            baos.flush();
            zos.close();
            baos.close();
            outputStream.write(baos.toByteArray());
            outputStream.flush();
            outputStream.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.    
        } catch (IOException e) {

        }

    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public String getReportRange() {
        return reportRange;
    }

    public void setReportRange(String reportRange) {
        this.reportRange = reportRange;
    }

    public List<String> getSelectedUnitateAttributes() {
        return selectedUnitateAttributes;
    }

    public void setSelectedUnitateAttributes(List<String> selectedUnitateAttributes) {
        this.selectedUnitateAttributes = selectedUnitateAttributes;
    }

}
