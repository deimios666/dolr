package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;
import ro.deimios.dolr.datamodel.LazyRegistruDataModelISJ;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@SessionScoped
public class DeleteBean implements Serializable {

    @EJB
    private RegistruFacadeLocal registruFacade;

    private LazyDataModel<Registru> registruList;
    private Registru selectedRegistru;
    private static final Logger LOG = Logger.getLogger(DeleteBean.class.getName());

    /**
     * Creates a new instance of DeleteBean
     */
    public DeleteBean() {
    }

    @PostConstruct
    public void loadData() {
        registruList = new LazyRegistruDataModelISJ();
    }

    public void doDelete() {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        if (userBean.ISJ) {
            try {
                registruFacade.deleteByNr(selectedRegistru.getNr());
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Șters cu succes", "Nr. " + selectedRegistru.getNr());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            } catch (NumberFormatException ex) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare ștergere", ex.toString());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare ștergere", "Nu sunteți autorizat pentru ștergere");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public LazyDataModel<Registru> getRegistruList() {
        return registruList;
    }

    public Registru getSelectedRegistru() {
        return selectedRegistru;
    }
    
    public void setSelectedRegistru(Registru selectedRegistru) {
        this.selectedRegistru = selectedRegistru;
        Registru registru = registruFacade.find(selectedRegistru.getNr());
    }
}
