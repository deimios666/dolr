package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import ro.deimios.dolr.datamodel.LazyRegistruDataModel;
import ro.deimios.dolr.datatype.UnitateRaspuns;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.facade.TransferFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class RegistruListerBean implements Serializable {

    @EJB
    private TransferFacadeLocal transferFacade;
    private LazyDataModel<Registru> registruList;
    private final SelectItem[] responseTypes;
    private static final Logger LOG = Logger.getLogger(RegistruListerBean.class.getName());

    /**
     * Creates a new instance of RegistruListerBean
     */
    public RegistruListerBean() {
        responseTypes = new SelectItem[4];
        responseTypes[0] = new SelectItem("", "Toate");
        responseTypes[1] = new SelectItem(UnitateRaspuns.STATUS_UNSENT, "Fără");
        responseTypes[2] = new SelectItem(UnitateRaspuns.STATUS_SENT_AFTER_DEADLINE, "Ieșit din termen");
        responseTypes[3] = new SelectItem(UnitateRaspuns.STATUS_SENT_BEFORE_DEADLINE, "La Termen");
    }

    @PostConstruct
    public void loadData() {
        //registruList = registruFacade.findAll();
        //Collections.reverse(registruList);
        registruList = new LazyRegistruDataModel();
    }

    public LazyDataModel<Registru> getRegistruList() {
        return registruList;
    }

    public SelectItem[] getResponseTypes() {
        return responseTypes;
    }

    public TransferFacadeLocal getTransferFacade() {
        return transferFacade;
    }

    public void setTransferFacade(TransferFacadeLocal transferFacade) {
        this.transferFacade = transferFacade;
    }
}
