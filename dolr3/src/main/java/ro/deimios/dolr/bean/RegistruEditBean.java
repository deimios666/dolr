package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import ro.deimios.dolr.datamodel.LazyRegistruDataModelISJ;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.entity.TipStat;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@SessionScoped
public class RegistruEditBean implements Serializable {

    @EJB
    private RegistruFacadeLocal registruFacade;

    @EJB
    private TransferFacadeLocal transferFacade;
    private LazyDataModel<Registru> registruList;
    private Registru selectedRegistru;
    private static final Logger LOG = Logger.getLogger(RegistruEditBean.class.getName());

    private String titlu;
    private String responsabil;
    private Date publishDate;
    private Date termen;
    private TipStat tipStat;

    /**
     * Creates a new instance of RegistruListerBean
     */
    public RegistruEditBean() {

    }

    @PostConstruct
    public void loadData() {
        registruList = new LazyRegistruDataModelISJ();
    }

    public void onRowSelect(SelectEvent event) {
    }

    public void doEdit() {
        Registru registru = registruFacade.find(selectedRegistru.getNr());
        registru.setShortname(this.titlu);
        registru.setResponsabil(this.responsabil);
        registru.setData(this.publishDate);
        registru.setTermen(this.termen);
        registru.setTipStat(this.tipStat);
        registruFacade.edit(registru);
    }

    public LazyDataModel<Registru> getRegistruList() {
        return registruList;
    }

    public TransferFacadeLocal getTransferFacade() {
        return transferFacade;
    }

    public void setTransferFacade(TransferFacadeLocal transferFacade) {
        this.transferFacade = transferFacade;
    }

    public Registru getSelectedRegistru() {
        return selectedRegistru;
    }

    public void setSelectedRegistru(Registru selectedRegistru) {
        this.selectedRegistru = selectedRegistru;
        Registru registru = registruFacade.find(selectedRegistru.getNr());
        this.titlu = registru.getShortname();
        this.responsabil = registru.getResponsabil();
        this.publishDate = registru.getData();
        this.termen = registru.getTermen();
        this.tipStat = registru.getTipStat();
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getResponsabil() {
        return responsabil;
    }

    public void setResponsabil(String responsabil) {
        this.responsabil = responsabil;
    }

    public Date getTermen() {
        return termen;
    }

    public void setTermen(Date termen) {
        this.termen = termen;
    }

    public TipStat getTipStat() {
        return tipStat;
    }

    public void setTipStat(TipStat tipStat) {
        this.tipStat = tipStat;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }
    
    

}
