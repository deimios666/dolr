package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ro.deimios.dolr.facade.UnitateAttribFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class ReportBean implements Serializable {

    @EJB
    private UnitateAttribFacadeLocal unitateAttribFacade;

    private String reportId;
    private String reportRange;
    private List<String> unitateAttributes;
    private List<String> selectedUnitateAttributes;

    @PostConstruct
    public void loadData() {
        unitateAttributes = unitateAttribFacade.getAttributeKeys();
    }

    /**
     * Creates a new instance of ReportBean
     */
    public ReportBean() {
    }

    public String getReportRange() {
        return reportRange;
    }

    public void setReportRange(String reportRange) {
        this.reportRange = reportRange;
    }

    public List<String> getUnitateAttributes() {
        return unitateAttributes;
    }

    public void setUnitateAttributes(List<String> unitateAttributes) {
        this.unitateAttributes = unitateAttributes;
    }

    public List<String> getSelectedUnitateAttributes() {
        return selectedUnitateAttributes;
    }

    public void setSelectedUnitateAttributes(List<String> selectedUnitateAttributes) {
        this.selectedUnitateAttributes = selectedUnitateAttributes;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

}
