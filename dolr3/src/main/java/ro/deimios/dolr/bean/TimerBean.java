package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author deimios
 */
@ManagedBean
@SessionScoped
public class TimerBean implements Serializable {

    private Date currentTime;

    /**
     * Creates a new instance of TimerBean
     */
    public TimerBean() {
        currentTime = new Date();
    }

    public void updateTime() {
        currentTime = new Date();
    }

    public String getTime() {
        return new SimpleDateFormat("HH:mm:ss").format(currentTime);
    }
}
