package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.chart.PieChartModel;
import ro.deimios.dolr.datamodel.LazyTransferForUserDataModel;
import ro.deimios.dolr.datamodel.LazyTransferForUserDataModelActual;
import ro.deimios.dolr.datamodel.LazyTransferForUserDataModelHistory;
import ro.deimios.dolr.datatype.UnitateRaspuns;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class UnitateRegistruListerBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(UnitateRegistruListerBean.class.getName());
    private final SelectItem[] responseTypes;
    @EJB
    private TransferFacadeLocal transferFacade;
    private LazyDataModel<Transfer> transferList;
    private LazyDataModel<Transfer> actualTransferList;
    private LazyDataModel<Transfer> historyTransferList;
    private long userUnitateCode;
    private long actual;
    private long expired;
    private PieChartModel model;

    /**
     * Creates a new instance of UnitateRegistruListerBean
     */
    public UnitateRegistruListerBean() {
        responseTypes = new SelectItem[4];
        responseTypes[0] = new SelectItem("", "Toate");
        responseTypes[1] = new SelectItem(UnitateRaspuns.STATUS_UNSENT, "Fără");
        responseTypes[2] = new SelectItem(UnitateRaspuns.STATUS_SENT_AFTER_DEADLINE, "Ieșit din termen");
        responseTypes[3] = new SelectItem(UnitateRaspuns.STATUS_SENT_BEFORE_DEADLINE, "La Termen");
    }

    @PostConstruct
    public void loadData() {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        userUnitateCode = userBean.user.getUnitate().getSirues();
        transferList = new LazyTransferForUserDataModel(userUnitateCode);
        actualTransferList = new LazyTransferForUserDataModelActual(userUnitateCode);
        historyTransferList = new LazyTransferForUserDataModelHistory(userUnitateCode);
        actual = transferFacade.countForUnitateStatus(userUnitateCode, 1, UnitateRaspuns.STATUS_UNSENT);
        expired = transferFacade.countForUnitateExpiredStatus(userUnitateCode, 1, UnitateRaspuns.STATUS_UNSENT);
        long[] respStat = transferFacade.getResponseStatisticsForUnitate(userUnitateCode);
        long complementaryPercent = respStat[1] - respStat[0];
        model = new PieChartModel();
        model.set("În Termen", respStat[0]);
        model.set("Ieșit din Termen", complementaryPercent);
        model.setSeriesColors("56c473,d23030");
        model.setShowDataLabels(true);
        model.setTitle("Situația Răspunsurilor");
        model.setLegendPosition("w");
    }

    public long getActual() {
        return actual;
    }

    public PieChartModel getModel() {
        return model;
    }
    public long getExpired() {
        return expired;
    }

    public LazyDataModel<Transfer> getTransferList() {
        return transferList;
    }

    public LazyDataModel<Transfer> getActualTransferList() {
        return actualTransferList;
    }

    public LazyDataModel<Transfer> getHistoryTransferList() {
        return historyTransferList;
    }

    public SelectItem[] getResponseTypes() {
        return responseTypes;
    }

    public TransferFacadeLocal getTransferFacade() {
        return transferFacade;
    }

    public void setTransferFacade(TransferFacadeLocal transferFacade) {
        this.transferFacade = transferFacade;
    }
}
