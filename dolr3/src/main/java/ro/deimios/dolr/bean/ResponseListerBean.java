package ro.deimios.dolr.bean;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.LazyDataModel;
import ro.deimios.dolr.datamodel.LazyTransferForRegistruDataModel;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.facade.RegistruFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@SessionScoped
public class ResponseListerBean implements Serializable {

    @EJB
    private RegistruFacadeLocal registruFacade;
    int selectedRegistruId;
    private String selectedRegistruShortName;
    private LazyDataModel<Transfer> transfers;

    /**
     * Creates a new instance of ResponseListerBean
     */
    public ResponseListerBean() {
    }

    public void loadTransferData() {
        this.transfers = new LazyTransferForRegistruDataModel(this.selectedRegistruId, 1);
    }

    public int getSelectedRegistruId() {
        return selectedRegistruId;
    }

    public void setSelectedRegistruId(int selectedRegistruId) {
        this.selectedRegistruId = selectedRegistruId;
        this.selectedRegistruShortName = registruFacade.find(selectedRegistruId).getShortname();
        loadTransferData();
        
    }

    public LazyDataModel<Transfer> getTransfers() {
        return transfers;
    }

    public String getSelectedRegistruShortName() {
        return selectedRegistruShortName;
    }

}
