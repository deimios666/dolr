package ro.deimios.dolr.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "registru")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registru.findAll", query = "SELECT r FROM Registru r"),
    @NamedQuery(name = "Registru.findByNr", query = "SELECT r FROM Registru r WHERE r.nr = :nr"),
    @NamedQuery(name = "Registru.findByData", query = "SELECT r FROM Registru r WHERE r.data = :data"),
    @NamedQuery(name = "Registru.deleteByNr", query = "DELETE FROM Registru r WHERE r.nr = :nr"),
    @NamedQuery(name = "Registru.findByShortname", query = "SELECT r FROM Registru r WHERE r.shortname = :shortname"),
    @NamedQuery(name = "Registru.findByTermen", query = "SELECT r FROM Registru r WHERE r.termen = :termen"),
    @NamedQuery(name = "Registru.findAllOrdered", query = "SELECT r FROM Registru r ORDER BY r.nr DESC"),
    @NamedQuery(name = "Registru.findActualOrdered", query = "SELECT r FROM Registru r WHERE r.termen >= CURRENT_TIMESTAMP ORDER BY r.nr DESC"),
    @NamedQuery(name = "Registru.findByTermenBeforeDate", query = "SELECT r FROM Registru r WHERE r.termen >= :termen")})
public class Registru implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "nr")
    private Integer nr;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "shortname")
    private String shortname;
    @Basic(optional = true)
    @Size(min = 1, max = 100)
    @Column(name = "responsabil")
    private String responsabil;
    @Basic(optional = false)
    @NotNull
    @Column(name = "termen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date termen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "registru")
    private List<Transfer> transferList;
    @JoinColumn(name = "tip_stat", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipStat tipStat;
    @Basic(optional = false)
    @Column(name = "nrtotal")
    private Integer nrtotal;
    @Basic(optional = false)
    @Column(name = "nrresponses")
    private Integer nrresponses;
    
    public Registru() {
    }

    public Registru(Integer nr) {
        this.nr = nr;
    }

    public Registru(Integer nr, Date data, String shortname, Date termen) {
        this.nr = nr;
        this.data = data;
        this.shortname = shortname;
        this.termen = termen;
    }

    public Integer getNr() {
        return nr;
    }

    public void setNr(Integer nr) {
        this.nr = nr;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public Date getTermen() {
        return termen;
    }

    public void setTermen(Date termen) {
        this.termen = termen;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    public TipStat getTipStat() {
        return tipStat;
    }

    public void setTipStat(TipStat tipStat) {
        this.tipStat = tipStat;
    }

    public String getResponsabil() {
        return responsabil;
    }

    public void setResponsabil(String responsabil) {
        this.responsabil = responsabil;
    }

    public Integer getNrtotal() {
        return nrtotal;
    }

    public void setNrtotal(Integer nrtotal) {
        this.nrtotal = nrtotal;
    }

    public Integer getNrresponses() {
        return nrresponses;
    }

    public void setNrresponses(Integer nrresponses) {
        this.nrresponses = nrresponses;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nr != null ? nr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registru)) {
            return false;
        }
        Registru other = (Registru) object;
        if ((this.nr == null && other.nr != null) || (this.nr != null && !this.nr.equals(other.nr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.Registru[ nr=" + nr + " ]";
    }
    
}
