package ro.deimios.dolr.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "unitate_attrib")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UnitateAttrib.findAll", query = "SELECT u FROM UnitateAttrib u"),
    @NamedQuery(name = "UnitateAttrib.findById", query = "SELECT u FROM UnitateAttrib u WHERE u.id = :id"),
    @NamedQuery(name = "UnitateAttrib.findByAttribKey", query = "SELECT u FROM UnitateAttrib u WHERE u.attribKey = :attribKey"),
    @NamedQuery(name = "UnitateAttrib.findForUnitateByAttribKey", query = "SELECT u FROM UnitateAttrib u WHERE u.attribKey = :attribKey AND u.unitateSirues.sirues = :sirues"),
    @NamedQuery(name = "UnitateAttrib.findByAttribValue", query = "SELECT u FROM UnitateAttrib u WHERE u.attribValue = :attribValue")})
public class UnitateAttrib implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "attrib_key")
    private String attribKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "attrib_value")
    private String attribValue;
    @JoinColumn(name = "unitate_sirues", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate unitateSirues;

    public UnitateAttrib() {
    }

    public UnitateAttrib(Integer id) {
        this.id = id;
    }

    public UnitateAttrib(Integer id, String attribKey, String attribValue) {
        this.id = id;
        this.attribKey = attribKey;
        this.attribValue = attribValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttribKey() {
        return attribKey;
    }

    public void setAttribKey(String attribKey) {
        this.attribKey = attribKey;
    }

    public String getAttribValue() {
        return attribValue;
    }

    public void setAttribValue(String attribValue) {
        this.attribValue = attribValue;
    }

    public Unitate getUnitateSirues() {
        return unitateSirues;
    }

    public void setUnitateSirues(Unitate unitateSirues) {
        this.unitateSirues = unitateSirues;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnitateAttrib)) {
            return false;
        }
        UnitateAttrib other = (UnitateAttrib) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.UnitateAttrib[ id=" + id + " ]";
    }
    
}
