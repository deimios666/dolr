package ro.deimios.dolr.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "transfer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transfer.findAll", query = "SELECT t FROM Transfer t"),
    @NamedQuery(name = "Transfer.findById", query = "SELECT t FROM Transfer t WHERE t.id = :id"),
    @NamedQuery(name = "Transfer.findBySentOn", query = "SELECT t FROM Transfer t WHERE t.sentOn = :sentOn"),
    @NamedQuery(name = "Transfer.findAllByRegistru", query = "SELECT t FROM Transfer t WHERE t.registru.nr=:registru"),
    @NamedQuery(name = "Transfer.findAllByRegistruByTip", query = "SELECT t FROM Transfer t WHERE t.registru.nr=:registru AND t.tipTransfer.id=:tiptransfer"),
    @NamedQuery(name = "Transfer.deleteByRegistru", query = "DELETE FROM Transfer t WHERE t.registru.nr=:registru"),
    @NamedQuery(name = "Transfer.findAllForUnitate", query = "SELECT t FROM Transfer t WHERE t.tipTransfer.id=:tiptransfer AND t.reciever.sirues=:sirues AND t.sentOn <= CURRENT_TIMESTAMP ORDER BY t.id DESC"),
    @NamedQuery(name = "Transfer.findResponses", query = "SELECT t FROM Transfer t WHERE t.raspunsLa.id = :id")
})
public class Transfer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "sent_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentOn;
    @Column(name = "rasp_status")
    private Integer raspStatus;
    @JoinColumn(name = "reciever", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate reciever;
    @JoinColumn(name = "sender", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate sender;
    @OneToMany(mappedBy = "raspunsLa", fetch = FetchType.EAGER)
    private List<Transfer> transferList;
    @JoinColumn(name = "raspuns_la", referencedColumnName = "id")
    @ManyToOne
    private Transfer raspunsLa;
    @JoinColumn(name = "tip_transfer", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipTransfer tipTransfer;
    @JoinColumn(name = "registru", referencedColumnName = "nr")
    @ManyToOne(optional = false)
    private Registru registru;
    @JoinColumn(name = "attachment", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private File attachment;
    @JoinColumn(name = "last_raspuns", referencedColumnName = "id")
    @ManyToOne
    private Transfer lastRaspuns;
    @Column(name = "rasp_count")
    private Integer raspCount;
    @Size(min = 1, max = 32)
    @Column(name = "pass")
    private String pass;

    public Transfer() {
    }

    public Transfer(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getSentOn() {
        return sentOn;
    }

    public void setSentOn(Date sentOn) {
        this.sentOn = sentOn;
    }

    public Integer getRaspStatus() {
        return raspStatus;
    }

    public void setRaspStatus(Integer raspStatus) {
        this.raspStatus = raspStatus;
    }

    public Unitate getReciever() {
        return reciever;
    }

    public void setReciever(Unitate reciever) {
        this.reciever = reciever;
    }

    public Unitate getSender() {
        return sender;
    }

    public void setSender(Unitate sender) {
        this.sender = sender;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    public Transfer getRaspunsLa() {
        return raspunsLa;
    }

    public void setRaspunsLa(Transfer raspunsLa) {
        this.raspunsLa = raspunsLa;
    }

    public TipTransfer getTipTransfer() {
        return tipTransfer;
    }

    public void setTipTransfer(TipTransfer tipTransfer) {
        this.tipTransfer = tipTransfer;
    }

    public Registru getRegistru() {
        return registru;
    }

    public void setRegistru(Registru registru) {
        this.registru = registru;
    }

    public File getAttachment() {
        return attachment;
    }

    public void setAttachment(File attachment) {
        this.attachment = attachment;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transfer)) {
            return false;
        }
        Transfer other = (Transfer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.Transfer[ id=" + id + " ]";
    }

    public Transfer getLastRaspuns() {
        return lastRaspuns;
    }

    public void setLastRaspuns(Transfer lastRaspuns) {
        this.lastRaspuns = lastRaspuns;
    }

    public Integer getRaspCount() {
        return raspCount;
    }

    public void setRaspCount(Integer raspCount) {
        this.raspCount = raspCount;
    }
}
