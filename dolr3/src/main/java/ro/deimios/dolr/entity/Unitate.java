package ro.deimios.dolr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "unitate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Unitate.findAll", query = "SELECT u FROM Unitate u"),
    @NamedQuery(name = "Unitate.findBySirues", query = "SELECT u FROM Unitate u WHERE u.sirues = :sirues"),
    @NamedQuery(name = "Unitate.findByNume", query = "SELECT u FROM Unitate u WHERE u.nume = :nume"),
    @NamedQuery(name = "Unitate.findAllNonISJPJ", query = "SELECT u FROM Unitate u WHERE u.tipUnitate.id<>4 AND u.sirues=u.superior.sirues ORDER BY u.nume")})
public class Unitate implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "sirues")
    private Long sirues;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "nume")
    private String nume;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reciever")
    private List<Transfer> transferList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sender")
    private List<Transfer> transferList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitate")
    private List<User> userList;
    @OneToMany(fetch=FetchType.EAGER,cascade = CascadeType.ALL, mappedBy = "superior")
    private List<Unitate> unitateList;
    @JoinColumn(name = "superior", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate superior;
    @JoinColumn(name = "tip_unitate", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipUnitate tipUnitate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitateSirues")
    private List<UnitateAttrib> unitateAttribList;


    public Unitate() {
    }

    public Unitate(Long sirues) {
        this.sirues = sirues;
    }

    public Unitate(Long sirues, String nume) {
        this.sirues = sirues;
        this.nume = nume;
    }

    public Long getSirues() {
        return sirues;
    }

    public void setSirues(Long sirues) {
        this.sirues = sirues;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @XmlTransient
    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    @XmlTransient
    public List<Transfer> getTransferList1() {
        return transferList1;
    }

    public void setTransferList1(List<Transfer> transferList1) {
        this.transferList1 = transferList1;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    public List<Unitate> getUnitateList() {
        return unitateList;
    }

    public void setUnitateList(List<Unitate> unitateList) {
        this.unitateList = unitateList;
    }

    public Unitate getSuperior() {
        return superior;
    }

    public void setSuperior(Unitate superior) {
        this.superior = superior;
    }

    public TipUnitate getTipUnitate() {
        return tipUnitate;
    }

    public void setTipUnitate(TipUnitate tipUnitate) {
        this.tipUnitate = tipUnitate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sirues != null ? sirues.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unitate)) {
            return false;
        }
        Unitate other = (Unitate) object;
        if ((this.sirues == null && other.sirues != null) || (this.sirues != null && !this.sirues.equals(other.sirues))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.Unitate[ sirues=" + sirues + " ]";
    }

    @XmlTransient
    public List<UnitateAttrib> getUnitateAttribList() {
        return unitateAttribList;
    }

    public void setUnitateAttribList(List<UnitateAttrib> unitateAttribList) {
        this.unitateAttribList = unitateAttribList;
    }
}
