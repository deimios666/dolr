package ro.deimios.dolr.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "filecontent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Filecontent.findAll", query = "SELECT f FROM Filecontent f")
    , @NamedQuery(name = "Filecontent.findById", query = "SELECT f FROM Filecontent f WHERE f.id = :id")})
public class Filecontent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    /*@Basic(fetch=LAZY)
    @Lob*/
    @Column(name = "filecontent")
    private byte[] filecontent;
    
    @JoinColumn(name = "file_id", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.ALL)
    private File fileId;

    public Filecontent() {
    }

    public Filecontent(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getFilecontent() {
        return filecontent;
    }

    public void setFilecontent(byte[] filecontent) {
        this.filecontent = filecontent;
    }

    public File getFile() {
        return fileId;
    }

    public void setFile(File fileId) {
        this.fileId = fileId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filecontent)) {
            return false;
        }
        Filecontent other = (Filecontent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.Filecontent[ id=" + id + " ]";
    }
    
}
