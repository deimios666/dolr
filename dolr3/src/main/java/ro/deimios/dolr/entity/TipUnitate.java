package ro.deimios.dolr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "tip_unitate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipUnitate.findAll", query = "SELECT t FROM TipUnitate t"),
    @NamedQuery(name = "TipUnitate.findById", query = "SELECT t FROM TipUnitate t WHERE t.id = :id"),
    @NamedQuery(name = "TipUnitate.findByNume", query = "SELECT t FROM TipUnitate t WHERE t.nume = :nume")})
public class TipUnitate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nume")
    private String nume;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipUnitate")
    private List<Unitate> unitateList;

    public TipUnitate() {
    }

    public TipUnitate(Integer id) {
        this.id = id;
    }

    public TipUnitate(Integer id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @XmlTransient
    public List<Unitate> getUnitateList() {
        return unitateList;
    }

    public void setUnitateList(List<Unitate> unitateList) {
        this.unitateList = unitateList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipUnitate)) {
            return false;
        }
        TipUnitate other = (TipUnitate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.TipUnitate[ id=" + id + " ]";
    }
    
}
