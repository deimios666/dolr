package ro.deimios.dolr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "tip_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipUser.findAll", query = "SELECT t FROM TipUser t"),
    @NamedQuery(name = "TipUser.findById", query = "SELECT t FROM TipUser t WHERE t.id = :id"),
    @NamedQuery(name = "TipUser.findByName", query = "SELECT t FROM TipUser t WHERE t.name = :name"),
    @NamedQuery(name = "TipUser.findByCanRespond", query = "SELECT t FROM TipUser t WHERE t.canRespond = :canRespond"),
    @NamedQuery(name = "TipUser.findByCanLogin", query = "SELECT t FROM TipUser t WHERE t.canLogin = :canLogin"),
    @NamedQuery(name = "TipUser.findByCanSend", query = "SELECT t FROM TipUser t WHERE t.canSend = :canSend"),
    @NamedQuery(name = "TipUser.findByCanManageAllUsers", query = "SELECT t FROM TipUser t WHERE t.canManageAllUsers = :canManageAllUsers"),
    @NamedQuery(name = "TipUser.findByCanManageOwnUsers", query = "SELECT t FROM TipUser t WHERE t.canManageOwnUsers = :canManageOwnUsers")})
public class TipUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_respond")
    private boolean canRespond;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_login")
    private boolean canLogin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_send")
    private boolean canSend;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_manage_all_users")
    private boolean canManageAllUsers;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_manage_own_users")
    private boolean canManageOwnUsers;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipUser")
    private List<User> userList;

    public TipUser() {
    }

    public TipUser(Integer id) {
        this.id = id;
    }

    public TipUser(Integer id, String name, boolean canRespond, boolean canLogin, boolean canSend, boolean canManageAllUsers, boolean canManageOwnUsers) {
        this.id = id;
        this.name = name;
        this.canRespond = canRespond;
        this.canLogin = canLogin;
        this.canSend = canSend;
        this.canManageAllUsers = canManageAllUsers;
        this.canManageOwnUsers = canManageOwnUsers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getCanRespond() {
        return canRespond;
    }

    public void setCanRespond(boolean canRespond) {
        this.canRespond = canRespond;
    }

    public boolean getCanLogin() {
        return canLogin;
    }

    public void setCanLogin(boolean canLogin) {
        this.canLogin = canLogin;
    }

    public boolean getCanSend() {
        return canSend;
    }

    public void setCanSend(boolean canSend) {
        this.canSend = canSend;
    }

    public boolean getCanManageAllUsers() {
        return canManageAllUsers;
    }

    public void setCanManageAllUsers(boolean canManageAllUsers) {
        this.canManageAllUsers = canManageAllUsers;
    }

    public boolean getCanManageOwnUsers() {
        return canManageOwnUsers;
    }

    public void setCanManageOwnUsers(boolean canManageOwnUsers) {
        this.canManageOwnUsers = canManageOwnUsers;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipUser)) {
            return false;
        }
        TipUser other = (TipUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.TipUser[ id=" + id + " ]";
    }
    
}
