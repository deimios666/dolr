package ro.deimios.dolr.datamodel;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.facade.RegistruFacadeLocal;

/**
 *
 * @author deimios
 */
public class LazyRegistruDataModelISJ extends LazyDataModel<Registru> {

    RegistruFacadeLocal registruFacade = lookupRegistruFacadeLocal();

    public LazyRegistruDataModelISJ() {
    }

    @Override
    public Registru getRowData(String rowKey) {
        return registruFacade.find(Integer.valueOf(rowKey));
    }

    @Override
    public Object getRowKey(Registru registru) {
        return registru.getNr();
    }

    @Override
    public List<Registru> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        this.setRowCount((int) registruFacade.countFilteredISJ(first, pageSize, sortField, sortOrder, filters));
        return registruFacade.findFilteredISJ(first, pageSize, sortField, sortOrder, filters);
    }
    private static final Logger LOG = Logger.getLogger(LazyRegistruDataModel.class.getName());

    @Override
    public void setRowIndex(int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    private RegistruFacadeLocal lookupRegistruFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (RegistruFacadeLocal) c.lookup("java:global/dolr3/RegistruFacade!ro.deimios.dolr.facade.RegistruFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
