package ro.deimios.dolr.datamodel;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.facade.TransferFacadeLocal;

/**
 *
 * @author deimios
 */
public class LazyTransferForRegistruDataModel extends LazyDataModel<Transfer> {

    private static final Logger LOG = Logger.getLogger(LazyTransferForRegistruDataModel.class.getName());
    private final int registruId;
    private final int tipTransferId;
    TransferFacadeLocal transferFacade = lookupTransferFacadeLocal();

    public LazyTransferForRegistruDataModel(int registruId,int tipTransferId) {
        this.registruId = registruId;
        this.tipTransferId = tipTransferId;
    }

       @Override
    public Transfer getRowData(String rowKey) {
        return transferFacade.find(Integer.valueOf(rowKey));
    }

    @Override
    public Object getRowKey(Transfer transfer) {
        return transfer.getId();
    }

    @Override
    public List<Transfer> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        this.setRowCount((int) transferFacade.countFilteredForRegistru(registruId,tipTransferId, first, pageSize, sortField, sortOrder, filters));
        return transferFacade.findFilteredForRegistru(registruId,tipTransferId, first, pageSize, sortField, sortOrder, filters);
    }

    @Override
    public void setRowIndex(int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    public int getRegistruId() {
        return registruId;
    }

    private TransferFacadeLocal lookupTransferFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (TransferFacadeLocal) c.lookup("java:global/dolr3/TransferFacade!ro.deimios.dolr.facade.TransferFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
