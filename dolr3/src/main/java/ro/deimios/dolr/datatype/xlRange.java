package ro.deimios.dolr.datatype;

import java.util.ArrayList;
import java.util.List;
import ro.deimios.dolr.helper.ExcelHelper;

/**
 *
 * @author Deimios
 */
public class xlRange {

    private int c1, c2, r1, r2;

    public xlRange() {
    }

    public xlRange(int c1, int c2, int r1, int r2) {
        this.c1 = c1;
        this.c2 = c2;
        this.r1 = r1;
        this.r2 = r2;
    }

    public xlRange(String range) {
        //is it a single letter or a range?
        String workingRange = range.toUpperCase().replaceAll("\\s+", "");
        if (workingRange.contains(":")) {
            //is a range
            String[] parts = workingRange.split(":");

            c1 = ExcelHelper.getColNrFromRef(parts[0]);
            r1 = ExcelHelper.getRowNr(parts[0]);
            c2 = ExcelHelper.getColNrFromRef(parts[1]);
            r2 = ExcelHelper.getRowNr(parts[1]);

        } else {
            c1 = ExcelHelper.getColNrFromRef(workingRange);
            r1 = ExcelHelper.getRowNr(workingRange);
            c2 = c1;
            r2 = r1;
        }
    }

    public List<xlCell> getCellList() {
        ArrayList retVal = new ArrayList<>();
        for (int rowWalker = r1; rowWalker < (r2 + 1); rowWalker++) {
            for (int colWalker = c1; colWalker < (c2 + 1); colWalker++) {
                retVal.add(new xlCell(colWalker, rowWalker));
            }
        }
        return retVal;
    }

    public int getC1() {
        return c1;
    }

    public void setC1(int c1) {
        this.c1 = c1;
    }

    public int getC2() {
        return c2;
    }

    public void setC2(int c2) {
        this.c2 = c2;
    }

    public int getR1() {
        return r1;
    }

    public void setR1(int r1) {
        this.r1 = r1;
    }

    public int getR2() {
        return r2;
    }

    public void setR2(int r2) {
        this.r2 = r2;
    }
}
