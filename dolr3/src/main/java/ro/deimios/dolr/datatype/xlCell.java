package ro.deimios.dolr.datatype;

import ro.deimios.dolr.helper.ExcelHelper;

/**
 *
 * @author Deimios
 */
public class xlCell {

    private int col;
    private int row;

    public xlCell(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public String getCellRef() {
        return ExcelHelper.numberToCharacterRepresentation(col) + row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }
}
