/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.datatype;

import java.util.Comparator;

/**
 *
 * @author Deimios
 */
public class xlCellComparator implements Comparator<xlCell> {

    @Override
    public int compare(xlCell c1, xlCell c2) {
        if (c1.getRow() < c2.getRow()) {
            return -1;
        } else if (c1.getRow() > c2.getRow()) {
            return 1;
        } else if (c1.getCol() < c2.getCol()) {
            return -1;
        } else if (c1.getCol() > c2.getCol()) {
            return 1;
        } else {
            return 0;
        }
    }
}
