package ro.deimios.dolr.helper;

/**
 *
 * @author deimios
 */
public class EncodingCheckHelper {
    public static String getEncodingInfo(){
        return "file.encoding: "+System.getProperty("file.encoding")+" / javax.servlet.request.encoding: "+System.getProperty("javax.servlet.request.encoding");
    }
}
