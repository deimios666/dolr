package ro.deimios.dolr.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author deimios
 */
public class FileHelper {

    private static final int BUFFER_SIZE = 8096;

    public static String sanitizeFileName(String fileName){
        return fileName.replaceAll("^[.\\\\/:*?\"<>|]?[\\\\/:*?\"<>|]*", "_");
    }
    
    public static File saveUploadedFile(InputStream inputStream, String fileName) throws FileNotFoundException, IOException {
        return saveUploadedFile(inputStream, fileName, true);
    }

    public static File saveUploadedFile(InputStream inputStream, String fileName, boolean skipExistingFile) throws FileNotFoundException, IOException {
        File outputFile = new File(fileName);
        if (!(outputFile.exists() && skipExistingFile)) {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            byte[] buffer = new byte[BUFFER_SIZE];
            int bulk;
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();
        }
        return outputFile;
    }
}
