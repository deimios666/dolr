package ro.deimios.dolr.helper;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ro.deimios.dolr.datatype.xlCell;
import ro.deimios.dolr.datatype.xlCellComparator;
import ro.deimios.dolr.entity.TipStat;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.entity.Unitate;
import ro.deimios.dolr.facade.FileFacadeLocal;
import ro.deimios.dolr.facade.UnitateFacadeLocal;

/**
 *
 * @author deimios
 */
public class ReportHelper {

    private static final Logger LOG = Logger.getLogger(ReportHelper.class.getName());

    public static Workbook createReport(ExternalContext ec, boolean isFixedTable, String reportRange, int reportTypeId, boolean isReportingOnSubUnit, List<Transfer> lastResponseList) {
        LOG.log(Level.FINEST, "Starting report creation");
        //create workbook in memory
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Sheet1");
        LOG.log(Level.FINEST, "Sheet created");
        //if stat type is variable then we can only flatten
        if (isFixedTable) {
            LOG.log(Level.FINEST, "Report range: {0}", reportRange);
            //range refers to actual data
            List<xlCell> cellList = ExcelHelper.refToCellList(reportRange);
            //sort order is row then column
            Collections.sort(cellList, new xlCellComparator());
            //reportTypeId can be 0 (copy with flatten) or 1(sum)
            if (reportTypeId == 0) {
                //flatten and copy
                int currentRow = 1;
                Row createdRow = sheet.createRow(currentRow);
                LOG.log(Level.FINEST, "Response list size: {0}", lastResponseList.size());
                for (Transfer lastResponseList1 : lastResponseList) {
                    InputStream fis;
                    try {
                        Transfer raspuns = lastResponseList1;
                        //get the file hash
                        String fileHash = raspuns.getAttachment().getHash();
                        //open file
                        fis = ec.getResourceAsStream("/WEB-INF/upload/" + fileHash);
                        if (null != fis) {
                            //open workbook
                            Workbook wb = WorkbookFactory.create(fis);
                            if (!isReportingOnSubUnit) {
                                //PJ
                                Sheet s = wb.getSheetAt(0);
                                Iterator<xlCell> cellWalker = cellList.iterator();
                                int currentCol = 2; //start from C, since A will have PJ sirues name and B will have PJ name
                                createdRow.createCell(0).setCellValue(raspuns.getSender().getSirues());
                                createdRow.createCell(1).setCellValue(raspuns.getSender().getNume());
                                while (cellWalker.hasNext()) {
                                    xlCell c = cellWalker.next();
                                    //copy data
                                    Cell originCell = s.getRow(c.getRow()).getCell(c.getCol());
                                    Cell createdCell = createdRow.createCell(currentCol);
                                    switch (originCell.getCellType()) {
                                        case Cell.CELL_TYPE_STRING:
                                            createdCell.setCellValue(originCell.getStringCellValue());
                                            break;
                                        case Cell.CELL_TYPE_NUMERIC:
                                            createdCell.setCellValue(originCell.getNumericCellValue());
                                            break;
                                        case Cell.CELL_TYPE_BOOLEAN:
                                            createdCell.setCellValue(originCell.getBooleanCellValue());
                                            break;
                                        case Cell.CELL_TYPE_FORMULA:
                                            createdCell.setCellValue(originCell.getCellFormula());
                                            break;
                                    }
                                    currentCol++;
                                }
                                currentRow++;
                                createdRow = sheet.createRow(currentRow);
                            } else {
                                //AR
                                //loop over all sheets
                                //get ar list
                                List<Unitate> arList = raspuns.getSender().getUnitateList();
                                Iterator<Unitate> arWalker = arList.iterator();
                                while (arWalker.hasNext()) {
                                    Unitate currentAr = arWalker.next();
                                    String sirues = String.valueOf(currentAr.getSirues());
                                    Sheet s = wb.getSheet(sirues);
                                    Iterator<xlCell> cellWalker = cellList.iterator();
                                    int currentCol = 4; //start from E
                                    createdRow.createCell(0).setCellValue(raspuns.getSender().getSirues());
                                    createdRow.createCell(1).setCellValue(raspuns.getSender().getNume());
                                    createdRow.createCell(2).setCellValue(currentAr.getSirues());
                                    createdRow.createCell(3).setCellValue(currentAr.getNume());
                                    while (cellWalker.hasNext()) {
                                        xlCell c = cellWalker.next();
                                        //copy data
                                        Cell originCell = s.getRow(c.getRow()).getCell(c.getCol());
                                        Cell createdCell = createdRow.createCell(currentCol);
                                        switch (originCell.getCellType()) {
                                            case Cell.CELL_TYPE_STRING:
                                                createdCell.setCellValue(originCell.getRichStringCellValue());
                                                break;
                                            case Cell.CELL_TYPE_NUMERIC:
                                                createdCell.setCellValue(originCell.getNumericCellValue());
                                                break;
                                            case Cell.CELL_TYPE_BOOLEAN:
                                                createdCell.setCellValue(originCell.getBooleanCellValue());
                                                break;
                                            case Cell.CELL_TYPE_FORMULA:
                                                createdCell.setCellValue(originCell.getCellFormula());
                                                break;
                                        }
                                        currentCol++;
                                    }
                                    currentRow++;
                                    createdRow = sheet.createRow(currentRow);
                                }
                            }
                            //close workbook reader
                            fis.close();
                        }
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ExcelHelper.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException | InvalidFormatException ex) {
                        Logger.getLogger(ExcelHelper.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                //sum
            }
        }
        if (!isFixedTable) {
            LOG.log(Level.FINEST, "Not a fixed table");
            //range refers to header , we need to scan for data UNDER this range
            //TODO implement variaable copy
        }
        return workbook;
    }

    //TODO implement createReport
    public static Workbook createReportNew(String reportRange, List<Object[]> data, TipStat tipStat, List<String> selectedColumns, ExternalContext ec, FileFacadeLocal fileFacade, UnitateFacadeLocal unitateFacade) {

        LOG.log(Level.FINEST, "Starting report creation");

        //prepare the copier
        List<xlCell> cellList = ExcelHelper.refToCellList(reportRange);
        Collections.sort(cellList, new xlCellComparator());

        //create workbook in memory
        Workbook outputWorkbook = new XSSFWorkbook();
        Sheet outputSheet = outputWorkbook.createSheet("Sheet1");
        int lastCopiedRow = 0;

        //check type, split into simple, 2D, catalog and PJ, AR
        LOG.log(Level.FINEST, "TipStat: {0}", tipStat.getNume());

        //for each response
        for (Object[] dbRow : data) {
            LOG.log(Level.INFO, "Processing file: {0}", dbRow[2]);
            String fileHash = (String) dbRow[2];
            //open file of response
            InputStream fis = new ByteArrayInputStream(CompressionHelper.decompress(fileFacade.findByHash(fileHash).getFilecontent().getFilecontent()));
            boolean fileExists = true;
            Workbook sourceWorkbook = null;
            try {
                sourceWorkbook = WorkbookFactory.create(fis);
            } catch (NullPointerException | IOException | InvalidFormatException ex) {
                fileExists = false;
                LOG.log(Level.INFO, "File not found: {0}", fileHash);
            }

            //check TipStat
            if (tipStat.getAr()) {
                //if AR get structuri from DB
                Long siruesPJ = (long) dbRow[0];
                List<Unitate> arList = unitateFacade.find(siruesPJ).getUnitateList();
                for (Unitate unitateAr : arList) {
                    //  loop structura sheets
                    Long siruesAR = unitateAr.getSirues();
                    String numeAR = unitateAr.getNume();
                    if (!tipStat.getD() && !tipStat.getVariable()) {
                        //  if simple copy into 1 row
                        lastCopiedRow = lastCopiedRow + 1;
                        Row newRow = outputSheet.createRow(lastCopiedRow);

                        newRow.createCell(0).setCellValue((long) dbRow[0]); //sirues PJ
                        newRow.createCell(1).setCellValue((String) dbRow[1]); //nume PJ
                        newRow.createCell(2).setCellValue(siruesAR); //sirues AR
                        newRow.createCell(3).setCellValue(numeAR); //nume AR
                        int lastColumn = 3;

                        if (dbRow.length > 3) {
                            for (int dbRowWalker = 3; dbRowWalker < dbRow.length; dbRowWalker++) {
                                lastColumn = lastColumn + 1;
                                newRow.createCell(lastColumn).setCellValue((String) dbRow[dbRowWalker]);
                            }
                        }

                        if (fileExists) {

                            String siruesARString = String.valueOf(siruesAR);
                            //Sheet sourceSheet = sourceWorkbook.getSheetAt(0);
                            try {
                                Sheet sourceSheet = sourceWorkbook.getSheet(siruesARString);

                                for (xlCell xCell : cellList) {
                                    lastColumn = lastColumn + 1;
                                    Cell originCell = sourceSheet.getRow(xCell.getRow()).getCell(xCell.getCol());
                                    Cell createdCell = newRow.createCell(lastColumn);

                                    if (originCell.getCellType() == Cell.CELL_TYPE_STRING) {
                                        createdCell.setCellValue(originCell.getStringCellValue());
                                    }

                                    if (originCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                        createdCell.setCellValue(originCell.getNumericCellValue());
                                    }

                                    if (originCell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
                                        createdCell.setCellValue(originCell.getBooleanCellValue());
                                    }

                                    if (originCell.getCellType() == Cell.CELL_TYPE_FORMULA) {

                                        if (originCell.getCachedFormulaResultType() == Cell.CELL_TYPE_STRING) {
                                            createdCell.setCellValue(originCell.getStringCellValue());
                                        }

                                        if (originCell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
                                            createdCell.setCellValue(originCell.getNumericCellValue());
                                        }

                                        if (originCell.getCachedFormulaResultType() == Cell.CELL_TYPE_BOOLEAN) {
                                            createdCell.setCellValue(originCell.getBooleanCellValue());
                                        }
                                    }

                                }
                            } catch (Exception ex) {
                                lastColumn = lastColumn + 1;
                                newRow.createCell(lastColumn).setCellValue(ex.getMessage());
                            }

                        } else {
                            lastColumn = lastColumn + 1;
                            newRow.createCell(lastColumn).setCellValue("FILE NOT FOUND!");
                        }
                    }
                }

                if (tipStat.getD() && !tipStat.getVariable()) {
                    //  if 2D, calc rowheight, copy data, save last copied target row index
                }

                if (!tipStat.getD() && tipStat.getVariable()) {
                    //  if catalog, get header from range, copy data, save last copied target row index
                }

            } else {
                //if PJ
                if (!tipStat.getD() && !tipStat.getVariable()) {
                    //  if simple copy into 1 row
                    lastCopiedRow = lastCopiedRow + 1;
                    Row newRow = outputSheet.createRow(lastCopiedRow);

                    newRow.createCell(0).setCellValue((long) dbRow[0]);
                    newRow.createCell(1).setCellValue((String) dbRow[1]);
                    int lastColumn = 1;

                    if (dbRow.length > 3) {
                        for (int dbRowWalker = 3; dbRowWalker < dbRow.length; dbRowWalker++) {
                            lastColumn = lastColumn + 1;
                            newRow.createCell(lastColumn).setCellValue((String) dbRow[dbRowWalker]);
                        }
                    }

                    if (fileExists) {

                        Sheet sourceSheet = sourceWorkbook.getSheetAt(0);

                        for (xlCell xCell : cellList) {
                            lastColumn = lastColumn + 1;
                            Cell originCell = sourceSheet.getRow(xCell.getRow()).getCell(xCell.getCol());
                            Cell createdCell = newRow.createCell(lastColumn);

                            if (originCell.getCellType() == Cell.CELL_TYPE_STRING) {
                                createdCell.setCellValue(originCell.getStringCellValue());
                            }

                            if (originCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                createdCell.setCellValue(originCell.getNumericCellValue());
                            }

                            if (originCell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
                                createdCell.setCellValue(originCell.getBooleanCellValue());
                            }

                            if (originCell.getCellType() == Cell.CELL_TYPE_FORMULA) {

                                if (originCell.getCachedFormulaResultType() == Cell.CELL_TYPE_STRING) {
                                    createdCell.setCellValue(originCell.getStringCellValue());
                                }

                                if (originCell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC) {
                                    createdCell.setCellValue(originCell.getNumericCellValue());
                                }

                                if (originCell.getCachedFormulaResultType() == Cell.CELL_TYPE_BOOLEAN) {
                                    createdCell.setCellValue(originCell.getBooleanCellValue());
                                }
                            }

                        }

                    } else {
                        lastColumn = lastColumn + 1;
                        newRow.createCell(lastColumn).setCellValue("FILE NOT FOUND!");
                    }
                }

                if (tipStat.getD() && !tipStat.getVariable()) {
                    //  if 2D, calc rowheight, copy data, save last copied target row index
                }

                if (!tipStat.getD() && tipStat.getVariable()) {
                    //  if catalog, get header from range, copy data, save last copied target row index
                }

            }

            if (fileExists) {
                try {
                    sourceWorkbook.close();
                } catch (NullPointerException | IOException ex) {

                }
            }
        }

        //copy data according to type
        return outputWorkbook;
    }

}
