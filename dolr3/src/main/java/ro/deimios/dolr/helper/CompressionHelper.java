package ro.deimios.dolr.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.UnsupportedOptionsException;
import org.tukaani.xz.XZInputStream;
import org.tukaani.xz.XZOutputStream;

/**
 *
 * @author deimios
 */
public final class CompressionHelper {

    public static byte[] compressBytes(byte[] input) {
        XZOutputStream compOut = null;
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(input);
            ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
            java.io.BufferedOutputStream outStream = new java.io.BufferedOutputStream(outBuffer);
            compOut = new XZOutputStream(outStream, new LZMA2Options(8));
            IOUtils.copy(inputStream, compOut);
            compOut.flush();
            compOut.close();
            return outBuffer.toByteArray();
        } catch (IOException ex) {
            System.out.println("Error compressing file");
        } finally {
            try {
                compOut.close();
            } catch (IOException ex) {
                System.out.println("Error closing compressing file");
            }
        }
        return null;
    }

    public static byte[] compress(InputStream input) throws UnsupportedOptionsException, IOException {
        XZOutputStream compOut;
        ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
        java.io.BufferedOutputStream outStream = new java.io.BufferedOutputStream(outBuffer);
        compOut = new XZOutputStream(outStream, new LZMA2Options(8));
        IOUtils.copy(input, compOut);
        compOut.flush();
        compOut.close();
        return outBuffer.toByteArray();
    }

    public static byte[] decompress(byte[] input) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(input);
            ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
            java.io.BufferedOutputStream outStream = new java.io.BufferedOutputStream(outBuffer);
            InputStream compIn = new XZInputStream(inputStream);
            IOUtils.copy(compIn, outStream);
            outStream.flush();
            outStream.close();
            return outBuffer.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
