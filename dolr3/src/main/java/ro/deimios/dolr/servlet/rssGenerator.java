package ro.deimios.dolr.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringEscapeUtils;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.entity.User;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.facade.UserFacadeLocal;
import ro.deimios.dolr.helper.CryptHelper;

/**
 *
 * @author Deimios
 */
@WebServlet(name = "rssGenerator", urlPatterns = {"/rss.xml"})
public class rssGenerator extends HttpServlet {

    @EJB
    private TransferFacadeLocal transferFacade;
    @EJB
    private UserFacadeLocal userFacade;
    @EJB
    private RegistruFacadeLocal registruFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            int uid = Integer.valueOf(request.getParameter("uid"));
            if (uid < 1) {
                return;
            }
            String hash = request.getParameter("hash");
            if (hash.length() != 128 || !hash.matches("[0-9a-f]{128}")) {
                return;
            }

            User user = userFacade.find(uid);
            if (user == null) {
                return;
            }

            String dbHash = CryptHelper.sha512(user.getUsername() + user.getPassword());
            if (!dbHash.equals(hash)) {
                return;
            }

            //build the URL for links
            String scheme = request.getScheme();             // http
            String serverName = request.getServerName();     // hostname.com
            int serverPort = request.getServerPort();        // 80
            String contextPath = request.getContextPath();   // /mywebapp
            String servletPath = request.getServletPath();   // /servlet/MyServlet

            String baseUrl = scheme + "://" + serverName + ":" + serverPort + contextPath;

            long sirues = user.getUnitate().getSirues();

            List<Transfer> transferList = transferFacade.findLastXForUnitate(sirues, 1, 20);
            //Collections.reverse(transferList);
            Date lastBuildDate = transferList.get(0).getSentOn();

            //get last change date
            SimpleDateFormat sdf = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);
            SimpleDateFormat roFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

            out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            out.println("<rss version=\"2.0\">");
            out.println("<channel>");
            out.println("<title>DOLR</title>");
            out.println("<description>Registrul On-Line ISJ</description>");
            out.println("<link>" + baseUrl + "</link>");
            out.println("<lastBuildDate>" + sdf.format(lastBuildDate) + "</lastBuildDate>");
            out.println("<ttl>30</ttl>");

            Iterator<Transfer> transferWalker = transferList.iterator();
            while (transferWalker.hasNext()) {
                Transfer transfer = transferWalker.next();

                String fileUrl = baseUrl + "/getFile?"
                        + "uid=" + uid
                        + "&tid=" + transfer.getId()
                        + "&fid=" + transfer.getAttachment().getHash()
                        + "&hash=" + hash;

                out.println("<item>");
                out.println("<title>" + transfer.getRegistru().getShortname() + "</title>");
                out.println("<description>"
                        + StringEscapeUtils.escapeXml("<b>Termen: "
                                + roFormat.format(transfer.getRegistru().getTermen())
                                + "</b><br />"
                                + "<a href=\"" + fileUrl + "\">Descarcă Macheta</a>"
                                + "</pre>")
                        + "</description>");
                out.println("<guid>" + transfer.getId() + "</guid>");
                out.println("<pubDate>" + sdf.format(transfer.getSentOn()) + "</pubDate>");
                out.println("</item>");
            }

            out.println("</channel>");
            out.println("</rss>");

        } catch (java.lang.NumberFormatException nfe) {
            if (request.getParameter("uid") == null) {

                List<Registru> registruList = registruFacade.findLastXByDate(20);
                
                Collections.reverse(registruList); //why do I need this when I have order by in the query
                
                Iterator<Registru> registruWalker = registruList.iterator();

                //build the URL for links
                String scheme = request.getScheme();             // http
                String serverName = request.getServerName();     // hostname.com
                int serverPort = request.getServerPort();        // 80
                String contextPath = request.getContextPath();   // /mywebapp
                String servletPath = request.getServletPath();   // /servlet/MyServlet

                String baseUrl = scheme + "://" + serverName + ":" + serverPort + contextPath;

                Collections.reverse(registruList);
                Date lastBuildDate = registruList.get(0).getData();

                //get last change date
                SimpleDateFormat sdf = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);
                SimpleDateFormat roFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

                out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
                out.println("<rss version=\"2.0\">");
                out.println("<channel>");
                out.println("<title>DOLR</title>");
                out.println("<description>Registrul On-Line ISJ</description>");
                out.println("<link>" + baseUrl + "</link>");
                out.println("<lastBuildDate>" + sdf.format(lastBuildDate) + "</lastBuildDate>");
                out.println("<ttl>30</ttl>");

                while (registruWalker.hasNext()) {
                    Registru registru = registruWalker.next();
                    out.println("<item>");
                    out.println("<title>" + registru.getShortname() + "</title>");
                    out.println("<description>"
                            + StringEscapeUtils.escapeXml("<b>Termen: "
                                    + roFormat.format(registru.getTermen())
                                    + "</b>"
                                    + "</pre>")
                            + "</description>");
                    out.println("<guid>" + registru.getNr() + "</guid>");
                    out.println("<pubDate>" + sdf.format(registru.getData()) + "</pubDate>");
                    out.println("</item>");
                }
                out.println("</channel>");
                out.println("</rss>");
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
