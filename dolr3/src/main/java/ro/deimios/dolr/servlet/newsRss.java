package ro.deimios.dolr.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.facade.RegistruFacadeLocal;

/**
 *
 * @author deimios
 */
@WebServlet(name = "newsRss", urlPatterns = {"/news/rss.xml"})
public class newsRss extends HttpServlet {

    @EJB
    private RegistruFacadeLocal registruFacade;

    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            //build the URL for links
            String scheme = request.getScheme();             // http
            String serverName = request.getServerName();     // hostname.com
            int serverPort = request.getServerPort();        // 80
            String contextPath = request.getContextPath();   // /mywebapp
            String servletPath = request.getServletPath();   // /servlet/MyServlet

            String baseUrl = scheme + "://" + serverName + ":" + serverPort + contextPath;

            List<Registru> registruList = registruFacade.findLastActualXByDate(30);
            
            
            
            //get last change date
            SimpleDateFormat sdf = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);
            SimpleDateFormat roFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

            out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            out.println("<rss version=\"2.0\">");
            out.println("<channel>");
            out.println("<title>DOLR</title>");
            out.println("<description>Registrul On-Line ISJ</description>");
            out.println("<link>" + baseUrl + "</link>");
            //out.println("<lastBuildDate>" + sdf.format(lastBuildDate) + "</lastBuildDate>");
            out.println("<ttl>30</ttl>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
