package ro.deimios.dolr.converter;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.dolr.entity.Unitate;
import ro.deimios.dolr.facade.UnitateFacadeLocal;

/**
 *
 * @author Deimios
 */
@FacesConverter(forClass = ro.deimios.dolr.entity.Unitate.class, value = "unitateConverter")
public class UnitateConverter implements Converter {

    UnitateFacadeLocal UnitateFacade1 = lookupUnitateFacadeLocal();
    UnitateFacadeLocal unitateFacade = lookupUnitateFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return unitateFacade.find(Long.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Unitate) value).getSirues());
    }

    private UnitateFacadeLocal lookupUnitateFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (UnitateFacadeLocal) c.lookup("java:global/dolr3/UnitateFacade!ro.deimios.dolr.facade.UnitateFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
