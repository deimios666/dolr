ALTER TABLE `dolr`.`unitate` 
DROP FOREIGN KEY `fk_unitate_unitate1`;

ALTER TABLE `dolr`.`transfer` 
DROP FOREIGN KEY `fk_transfer_unitate1`,
DROP FOREIGN KEY `fk_transfer_unitate2`;

ALTER TABLE `dolr`.`user` 
DROP FOREIGN KEY `fk_user_unitate1`;

CREATE TABLE `dolr`.`unitate_attrib` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `attrib_key` VARCHAR(1024) NOT NULL DEFAULT 'key',
  `attrib_value` VARCHAR(1024) NOT NULL DEFAULT 'value',
  `unitate_sirues` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id`);


ALTER TABLE `dolr`.`unitate` 
CHANGE COLUMN `sirues` `sirues` BIGINT NOT NULL ,
CHANGE COLUMN `superior` `superior` BIGINT NOT NULL ;

ALTER TABLE `dolr`.`transfer` 
CHANGE COLUMN `sender` `sender` BIGINT NOT NULL ,
CHANGE COLUMN `reciever` `reciever` BIGINT NOT NULL ,
ADD COLUMN `last_raspuns` INT NULL DEFAULT NULL AFTER `rasp_status`,
ADD COLUMN `rasp_count` INT(11) NULL DEFAULT 0 AFTER `last_raspuns`;

ALTER TABLE `dolr`.`user` 
CHANGE COLUMN `unitate` `unitate` BIGINT NOT NULL ;

ALTER TABLE `dolr`.`unitate` 
ADD CONSTRAINT `fk_unitate_unitate1`
  FOREIGN KEY (`superior`)
  REFERENCES `dolr`.`unitate` (`sirues`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `dolr`.`transfer` 
ADD CONSTRAINT `fk_transfer_unitate1`
  FOREIGN KEY (`sender`)
  REFERENCES `dolr`.`unitate` (`sirues`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_transfer_unitate2`
  FOREIGN KEY (`reciever`)
  REFERENCES `dolr`.`unitate` (`sirues`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_transfer_raspuns`
  FOREIGN KEY (`last_raspuns`)
  REFERENCES `dolr`.`transfer` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `dolr`.`unitate_attrib` 
ADD CONSTRAINT `fk_unitate_attrib`
    FOREIGN KEY (`unitate_sirues`)
    REFERENCES `dolr`.`unitate` (`sirues`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `dolr`.`user` 
ADD CONSTRAINT `fk_user_unitate1`
  FOREIGN KEY (`unitate`)
  REFERENCES `dolr`.`unitate` (`sirues`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `dolr`.`filecontent` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `filecontent` LONGBLOB NULL,
  `file_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fi_con_fi_id_idx` (`file_id` ASC),
  CONSTRAINT `fi_con_fi_id`
    FOREIGN KEY (`file_id`)
    REFERENCES `dolr`.`file` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
