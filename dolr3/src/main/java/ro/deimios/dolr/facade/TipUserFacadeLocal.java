/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.TipUser;

/**
 *
 * @author Deimios
 */
@Local
public interface TipUserFacadeLocal {

    void create(TipUser tipUser);

    void edit(TipUser tipUser);

    void remove(TipUser tipUser);

    TipUser find(Object id);

    List<TipUser> findAll();

    List<TipUser> findRange(int[] range);

    int count();
    
}
