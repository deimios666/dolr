package ro.deimios.dolr.facade;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import org.primefaces.model.SortOrder;
import ro.deimios.dolr.entity.Registru;

/**
 *
 * @author Deimios
 */
@Local
public interface RegistruFacadeLocal {

    void create(Registru registru);

    void edit(Registru registru);

    void remove(Registru registru);

    Registru find(Object id);

    List<Registru> findAll();

    List<Registru> findRange(int[] range);

    int count();

    public void deleteByNr(int nr);

    public List<Registru> findByTermenBeforeDate(Date date);

    public List<Registru> findFiltered(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);
    
    public List<Registru> findFilteredISJ(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public long countFiltered(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);
    
    public long countFilteredISJ(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public List<Registru> findLastXByDate(int numberOfResults);

    public List<Registru> findLastActualXByDate(int numberOfResults);
    
}
