/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.Filecontent;

/**
 *
 * @author deimios
 */
@Local
public interface FilecontentFacadeLocal {

    void create(Filecontent filecontent);

    void edit(Filecontent filecontent);

    void remove(Filecontent filecontent);

    Filecontent find(Object id);

    List<Filecontent> findAll();

    List<Filecontent> findRange(int[] range);

    int count();
    
}
