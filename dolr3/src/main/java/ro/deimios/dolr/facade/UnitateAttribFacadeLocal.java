package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.UnitateAttrib;

/**
 *
 * @author deimios
 */
@Local
public interface UnitateAttribFacadeLocal {

    void create(UnitateAttrib unitateAttrib);

    void edit(UnitateAttrib unitateAttrib);

    void remove(UnitateAttrib unitateAttrib);

    UnitateAttrib find(Object id);

    List<UnitateAttrib> findAll();

    List<UnitateAttrib> findRange(int[] range);

    int count();

    public List<String> getAttributeKeys();
    
}
