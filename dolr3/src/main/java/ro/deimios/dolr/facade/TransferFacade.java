package ro.deimios.dolr.facade;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.primefaces.model.SortOrder;
import ro.deimios.dolr.entity.Transfer;

/**
 *
 * @author Deimios
 */
@Stateless
public class TransferFacade extends AbstractFacade<Transfer> implements TransferFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TransferFacade() {
        super(Transfer.class);
    }

    @Override
    public List<Transfer> findAllByRegistru(int registruId) {
        Query query = em.createNamedQuery("Transfer.findAllByRegistru");
        query.setParameter("registru", registruId);
        return query.getResultList();
    }
    
    @Override
    public List<Transfer> findAllByRegistruByTip(int registruId, int tipTransferId) {
        Query query = em.createNamedQuery("Transfer.findAllByRegistruByTip");
        query.setParameter("registru", registruId);
        query.setParameter("tiptransfer", tipTransferId);
        return query.getResultList();
    }

    @Override
    public List<Transfer> findFilteredForUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        String queryString = "select t from Transfer t";

        Iterator<String> filterWalker = filters.keySet().iterator();

        queryString += " where t.reciever.sirues=" + unitateId;

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                queryString += " and concat(t." + filterProperty + " ,'') like '%" + filterValue + "%'";
            }
        }
        
        queryString += " and t.sentOn <= CURRENT_TIMESTAMP";

        if (sortField != null) {
            queryString += " order by t." + sortField;
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " desc";
            } else {
                queryString += " asc";
            }
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);

        return query.getResultList();
    }
    private static final Logger LOG = Logger.getLogger(TransferFacade.class.getName());

    @Override
    public long countFilteredForUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String queryString = "select count(t) from Transfer t";

        Iterator<String> filterWalker = filters.keySet().iterator();

        queryString += " where t.reciever.sirues=" + unitateId;

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                queryString += " and concat(t." + filterProperty + " ,'') like '%" + filterValue + "%'";
            }
        }
        
        queryString += " and t.sentOn <= CURRENT_TIMESTAMP";

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }

    @Override
    public List<Transfer> findFilteredFromUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        String queryString = "select t from Transfer t";

        Iterator<String> filterWalker = filters.keySet().iterator();

        queryString += " where t.sender.sirues=" + unitateId;

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                queryString += " and concat(t." + filterProperty + " ,'') like '%" + filterValue + "%'";
            }
        }

        if (sortField != null) {
            queryString += " order by t." + sortField;
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " desc";
            } else {
                queryString += " asc";
            }
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);

        return query.getResultList();
    }

    @Override
    public long countFilteredFromUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String queryString = "select count(t) from Transfer t";

        Iterator<String> filterWalker = filters.keySet().iterator();

        queryString += " where t.sender.sirues=" + unitateId;

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                queryString += " and concat(t." + filterProperty + " ,'') like '%" + filterValue + "%'";
            }
        }

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }

    @Override
    public List<Transfer> findFilteredForRegistru(long registruId, int tipTransferId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        //SELECT t FROM Transfer t WHERE t.registru.nr=:registru AND t.tipTransfer.id=:tiptransfer
        String queryString = "select t from Transfer t";

        Iterator<String> filterWalker = filters.keySet().iterator();

        queryString += " where t.registru.nr=" + registruId + " and t.tipTransfer.id=" + tipTransferId;

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                queryString += " and concat(t." + filterProperty + " ,'') like '%" + filterValue + "%'";
            }
        }

        if (sortField != null) {
            queryString += " order by t." + sortField;
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " desc";
            } else {
                queryString += " asc";
            }
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);

        return query.getResultList();
    }

    @Override
    public long countFilteredForRegistru(long registruId, int tipTransferId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String queryString = "select count(t) from Transfer t";

        Iterator<String> filterWalker = filters.keySet().iterator();

        queryString += " where t.registru.nr=" + registruId + " and t.tipTransfer.id=" + tipTransferId;

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                queryString += " and concat(t." + filterProperty + " ,'') like '%" + filterValue + "%'";
            }
        }

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }

    @Override
    public List<Object[]> getResponseStatusForRegistru(int registruId) {
        String queryString = "select count(CASE WHEN t.raspStatus = 0 THEN 1 ELSE NULL END) AS Fără,"
                + "count(CASE WHEN t.raspStatus = 1 THEN 1 ELSE NULL END) AS LaTimp,"
                + "count(CASE WHEN t.raspStatus = 2 THEN 1 ELSE NULL END) AS Întârziat"
                + " from Transfer t where t.tipTransfer.id=1 and t.registru.nr=" + registruId + " group by t.registru.nr";
        Query query = em.createQuery(queryString);
        return query.getResultList();
    }

    @Override
    public List<Transfer> findAllForUnitate(long sirues, int tipTrandsferId) {
        Query query = em.createNamedQuery("Transfer.findAllForUnitate");
        query.setParameter("sirues", sirues);
        query.setParameter("tiptransfer", tipTrandsferId);
        return query.getResultList();
    }

    @Override
    public long[] getResponseStatisticsForUnitate(long sirues) {
        long[] retVal = new long[2];
        String queryString = "SELECT t.reciever,SUM(CASE WHEN t.raspStatus = 1 THEN 1 ELSE 0 END) AS LaTimp,"
                + "count(t) AS Total"
                + " from Transfer t where t.tipTransfer.id=1 and t.reciever.sirues=" + sirues
                + " and t.registru.termen < CURRENT_TIMESTAMP "
                + " group by t.reciever";
        Query query = em.createQuery(queryString);
        List resultList = query.getResultList();
        if (resultList.size() > 0) {
            Object[] answer = (Object[]) resultList.get(0);
            retVal[0] = ((Integer)answer[1]).longValue();
            retVal[1] = (Long) answer[2];
            
        }
        return retVal;
    }
    
    @Override
    public List<Object[]> getResponseStatistics() {
        String queryString = "SELECT t.reciever.nume,SUM(CASE WHEN t.raspStatus = 1 THEN 1 ELSE 0 END) AS LaTimp,"
                + "count(t) AS Total"
                + " from Transfer t where t.tipTransfer.id=1 "
                + " and t.registru.termen < CURRENT_TIMESTAMP "
                + " group by t.reciever.nume";
        Query query = em.createQuery(queryString);
        List resultList = query.getResultList();
        return resultList;
    }

    @Override
    public List<Transfer> findLastXForUnitate(long sirues, int tipTrandsferId, int numberOfResults) {
        Query query = em.createNamedQuery("Transfer.findAllForUnitate");
        query.setParameter("sirues", sirues);
        query.setParameter("tiptransfer", tipTrandsferId);
        query.setMaxResults(numberOfResults);
        return query.getResultList();
    }

    //@NamedQuery(name = "Transfer.findAllForUnitate", query = "SELECT t FROM Transfer t WHERE t.tipTransfer.id=:tiptransfer AND t.reciever.sirues=:sirues"),
    @Override
    public long countForUnitateStatus(long sirues, int tipTrandsferId, int status) {
        String queryString = "SELECT count(t) FROM Transfer t WHERE t.tipTransfer.id=:tiptransfer AND t.reciever.sirues=:sirues AND t.raspStatus=:status";
        Query query = em.createQuery(queryString);
        query.setParameter("sirues", sirues);
        query.setParameter("tiptransfer", tipTrandsferId);
        query.setParameter("status", status);
        return ((Number) query.getResultList().get(0)).longValue();
    }

    @Override
    public long countForUnitateExpiredStatus(long sirues, int tipTrandsferId, int status) {
        String queryString = "SELECT count(t) FROM Transfer t WHERE t.tipTransfer.id=:tiptransfer AND t.reciever.sirues=:sirues AND t.raspStatus=:status AND t.registru.termen<:today";
        Query query = em.createQuery(queryString);
        query.setParameter("sirues", sirues);
        query.setParameter("tiptransfer", tipTrandsferId);
        query.setParameter("status", status);
        query.setParameter("today", new Date(), TemporalType.DATE);

        return ((Number) query.getResultList().get(0)).longValue();
    }

    @Override
    public List<Object[]> getLastResponsesForRegistry(int registryId, List<String> attributes) {
        //select u.sirues,u.nume,fa.hash from Transfer t join t.lastRaspuns lr join lr.sender u join lr.attachment fa where t.registru.nr=228
        //select u,a1.attribValue from Unitate u join u.unitateAttribList a1 on a1.attribKey='sirues'
        String querySelect = "select u.sirues,u.nume,fa.hash";
        String queryJoin = " from Transfer t join t.lastRaspuns lr join lr.sender u join lr.attachment fa";
        String queryWhere = " where t.registru.nr=" + registryId;

        for (int i = 0; i < attributes.size(); i++) {
            querySelect += ",a" + i + ".attribValue";
            queryJoin += " join u.unitateAttribList a" + i;
            queryWhere += " AND a" + i + ".attribKey='" + attributes.get(i) + "'";
        }

        String queryString = querySelect + queryJoin + queryWhere;
        LOG.finer(queryString);
        Query query = em.createQuery(queryString);
        return query.getResultList();
    }

    @Override
    public void deleteByRegistruByTip(int registruId) {
        Query query = em.createNamedQuery("Transfer.deleteByRegistru");
        query.setParameter("registru", registruId);
        query.executeUpdate();
    }

    @Override
    public List<Transfer> findResponses(int id) {
        Query query = em.createNamedQuery("Transfer.findResponses");
        query.setParameter("id", id);
        return query.getResultList();
    }

}
