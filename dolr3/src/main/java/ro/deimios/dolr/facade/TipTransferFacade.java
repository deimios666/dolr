/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dolr.entity.TipTransfer;

/**
 *
 * @author Deimios
 */
@Stateless
public class TipTransferFacade extends AbstractFacade<TipTransfer> implements TipTransferFacadeLocal {
    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipTransferFacade() {
        super(TipTransfer.class);
    }
    
}
