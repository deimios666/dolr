/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dolr.entity.TipStat;

/**
 *
 * @author Deimios
 */
@Stateless
public class TipStatFacade extends AbstractFacade<TipStat> implements TipStatFacadeLocal {
    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipStatFacade() {
        super(TipStat.class);
    }
    
}
