package ro.deimios.dolr.facade;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.primefaces.model.SortOrder;
import ro.deimios.dolr.entity.Registru;

/**
 *
 * @author Deimios
 */
@Stateless
public class RegistruFacade extends AbstractFacade<Registru> implements RegistruFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RegistruFacade() {
        super(Registru.class);
    }

    @Override
    public void deleteByNr(int nr) {
        Query query = em.createNamedQuery("Registru.deleteByNr");
        query.setParameter("nr", nr);
        query.executeUpdate();

    }

    @Override
    public List<Registru> findByTermenBeforeDate(Date date) {
        Query query = em.createNamedQuery("Registru.findByTermenBeforeDate");
        query.setParameter("termen", date, TemporalType.DATE);
        return query.getResultList();
    }

    @Override
    public List<Registru> findLastXByDate(int numberOfResults) {
        Query query = em.createNamedQuery("Registru.findAllOrdered");
        query.setMaxResults(numberOfResults);
        return query.getResultList();
    }

    @Override
    public List<Registru> findLastActualXByDate(int numberOfResults) {
        Query query = em.createNamedQuery("Registru.findActualOrdered");
        query.setMaxResults(numberOfResults);
        return query.getResultList();
    }

    @Override
    public List<Registru> findFiltered(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String queryString = "select r from Registru r";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filters.keySet().iterator();

        if (!hasFilter) {
            queryString += " where r.data <= CURRENT_TIMESTAMP";
            hasFilter = true;
        }

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where";
                    hasFilter = true;
                } else {
                    queryString += " and";
                }
                queryString += " concat(r." + filterProperty + ",'') like '%" + filterValue + "%'";
            }

        }

        if (sortField != null) {
            queryString += " order by r." + sortField;
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " desc";
            } else {
                queryString += " asc";
            }
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }
    private static final Logger LOG = Logger.getLogger(RegistruFacade.class.getName());

    @Override
    public long countFiltered(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String queryString = "select count(r) from Registru r";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filters.keySet().iterator();

        if (!hasFilter) {
            queryString += " where r.data <= CURRENT_TIMESTAMP";
            hasFilter = true;
        }

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where";
                    hasFilter = true;
                } else {
                    queryString += " and";
                }
                queryString += " concat(r." + filterProperty + ",'') like '%" + filterValue + "%'";
            }
        }

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }

    @Override
    public List<Registru> findFilteredISJ(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String queryString = "select r from Registru r";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filters.keySet().iterator();

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where";
                    hasFilter = true;
                } else {
                    queryString += " and";
                }
                queryString += " concat(r." + filterProperty + ",'') like '%" + filterValue + "%'";
            }

        }

        if (sortField != null) {
            queryString += " order by r." + sortField;
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " desc";
            } else {
                queryString += " asc";
            }
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    @Override
    public long countFilteredISJ(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String queryString = "select count(r) from Registru r";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filters.keySet().iterator();

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filters.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where";
                    hasFilter = true;
                } else {
                    queryString += " and";
                }
                queryString += " concat(r." + filterProperty + ",'') like '%" + filterValue + "%'";
            }
        }

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }

}
