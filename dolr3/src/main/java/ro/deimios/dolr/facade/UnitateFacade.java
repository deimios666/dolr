/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.Unitate;

/**
 *
 * @author Deimios
 */
@Stateless
public class UnitateFacade extends AbstractFacade<Unitate> implements UnitateFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UnitateFacade() {
        super(Unitate.class);
    }

    @Override
    public List<Unitate> findAllNonISJPJ() {
        Query query = em.createNamedQuery("Unitate.findAllNonISJPJ");
        return query.getResultList();
    }

    @Override
    public List<Object[]> findAllNonISJPJWithAttributes() {
        Query attribsQuery = em.createQuery("SELECT DISTINCT(a.attribKey) FROM UnitateAttrib a");
        List<String> attribList = attribsQuery.getResultList();

        String bigQueryPrefix1 = "SELECT u";
        String bigQueryPrefix2 = " FROM Unitate u";
        String bigQuerySuffix1 = " WHERE u.tipUnitate.id<>4";
        String bigQuerySuffix2 = " AND u.sirues=u.superior.sirues ORDER BY u.nume";
                

        for (String attrib : attribList) {
            bigQueryPrefix1 = bigQueryPrefix1 + ", " + attrib;// + ".attribValue";
            bigQueryPrefix2 = bigQueryPrefix2 + " JOIN u.unitateAttribList " + attrib;
            bigQuerySuffix1 = bigQuerySuffix1 + " AND " + attrib + ".attribKey=\"" + attrib + "\"";
        }

        //System.out.println(bigQueryPrefix1 + bigQueryPrefix2 + bigQuerySuffix1 + bigQuerySuffix2);
        Query bigQuery = em.createQuery(bigQueryPrefix1 + bigQueryPrefix2 + bigQuerySuffix1 + bigQuerySuffix2);
        return bigQuery.getResultList();
    }
}
