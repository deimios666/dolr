package ro.deimios.dolr.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dolr.entity.Filecontent;

/**
 *
 * @author deimios
 */
@Stateless
public class FilecontentFacade extends AbstractFacade<Filecontent> implements FilecontentFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FilecontentFacade() {
        super(Filecontent.class);
    }
    
}
