/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.File;

/**
 *
 * @author Deimios
 */
@Stateless
public class FileFacade extends AbstractFacade<File> implements FileFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FileFacade() {
        super(File.class);
    }

    @Override
    public File findByHash(String hash) {
        Query query = em.createNamedQuery("File.findByHash");
        query.setParameter("hash", hash);
        List<File> resultList = query.getResultList();
        if (resultList == null || resultList.size() != 1) {
            return null;
        }
        return resultList.get(0);
    }
}
