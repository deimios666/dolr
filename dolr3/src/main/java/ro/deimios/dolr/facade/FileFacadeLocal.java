/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.File;

/**
 *
 * @author Deimios
 */
@Local
public interface FileFacadeLocal {

    void create(File file);

    void edit(File file);

    void remove(File file);

    File find(Object id);

    List<File> findAll();

    List<File> findRange(int[] range);

    int count();

    public ro.deimios.dolr.entity.File findByHash(java.lang.String hash);
}
