package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.UnitateAttrib;

/**
 *
 * @author deimios
 */
@Stateless
public class UnitateAttribFacade extends AbstractFacade<UnitateAttrib> implements UnitateAttribFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UnitateAttribFacade() {
        super(UnitateAttrib.class);
    }

    @Override
    public List<String> getAttributeKeys() {
        Query query = em.createQuery("select DISTINCT(ua.attribKey) from UnitateAttrib ua");
        List<String> result = query.getResultList();
        return result;
    }

}
