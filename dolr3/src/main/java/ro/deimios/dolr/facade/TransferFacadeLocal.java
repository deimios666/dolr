package ro.deimios.dolr.facade;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import org.primefaces.model.SortOrder;
import ro.deimios.dolr.entity.Transfer;

/**
 *
 * @author Deimios
 */
@Local
public interface TransferFacadeLocal {

    void create(Transfer transfer);

    void edit(Transfer transfer);

    void remove(Transfer transfer);

    Transfer find(Object id);

    List<Transfer> findAll();

    List<Transfer> findRange(int[] range);

    int count();

    public java.util.List<ro.deimios.dolr.entity.Transfer> findAllByRegistruByTip(int registruId, int tipTransferId);

    public java.util.List<ro.deimios.dolr.entity.Transfer> findAllForUnitate(long sirues, int tipTrandsferId);

    public java.util.List<ro.deimios.dolr.entity.Transfer> findResponses(int id);

    public void deleteByRegistruByTip(int registruId);

    public List<Transfer> findFilteredForUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public long countFilteredForUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public long countFilteredForRegistru(long registruId, int tipTransferId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public List<Transfer> findFilteredForRegistru(long registruId, int tipTransferId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public List<Object[]> getResponseStatusForRegistru(int registruId);

    public long countForUnitateStatus(long sirues, int tipTrandsferId, int status);

    public long countForUnitateExpiredStatus(long sirues, int tipTrandsferId, int status);

    public List<Object[]> getLastResponsesForRegistry(int registryId, List<String> attributes);

    public List<Transfer> findLastXForUnitate(long sirues, int tipTrandsferId, int numberOfResults);

    public List<Transfer> findFilteredFromUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public long countFilteredFromUnitate(long unitateId, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);

    public long[] getResponseStatisticsForUnitate(long sirues);

    public List<Transfer> findAllByRegistru(int registruId);

    public List<Object[]> getResponseStatistics();
    
}
