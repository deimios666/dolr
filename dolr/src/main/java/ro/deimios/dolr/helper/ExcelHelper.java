package ro.deimios.dolr.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import ro.deimios.dolr.datatype.xlCell;
import ro.deimios.dolr.datatype.xlRange;

/**
 *
 * @author Deimios
 */
public class ExcelHelper {

    private static final Logger LOG = Logger.getLogger(ExcelHelper.class.getName());

    public static int getColNrFromRef(String colRef) {
        String workingRef = colRef.toUpperCase().replaceAll("[^A-Z]+", "");
        return stringToNumber(workingRef) - 1;
    }

    public static int getRowNr(String cellRef) {
        return Integer.valueOf(cellRef.replaceAll("[^0-9]+", "")) - 1;
    }

    public static int stringToNumber(String str) {
        char[] ls = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        Map<Character, Integer> m = new HashMap<>();
        int j = 1;
        for (char c : ls) {
            m.put(c, j++);
        }
        int i = 0;
        int mul = 1;
        for (char c : new StringBuffer(str).reverse().toString().toCharArray()) {
            i += m.get(c) * mul;
            mul *= ls.length;
        }
        return i;
    }

    public static String numberToCharacterRepresentation(int number) {
        char[] ls = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        String r = "";
        while (true) {
            r = ls[number % 26] + r;
            if (number < 26) {
                break;
            }
            number /= 26;
        }
        return r;
    }

    public static List<xlCell> refToCellList(String ref) {
        List retVal = new ArrayList<>();
        String workingRef = ref.toUpperCase().replaceAll("\\s+", "");
        if (workingRef.indexOf(",") != -1) {
            //multiple range
            String[] parts = workingRef.split(",");
            for (int i = 0; i < parts.length; i++) {
                retVal.addAll(new xlRange(parts[i]).getCellList());
            }
        } else {
            //single range
            retVal.addAll(new xlRange(workingRef).getCellList());
        }
        return retVal;
    }

    public static String getExt(String fileName) {
        if (fileName.endsWith("xlsx")) {
            return "xlsx";
        } else if (fileName.endsWith("xls")) {
            return "xls";
        } else {
            return null;
        }
    }

}
