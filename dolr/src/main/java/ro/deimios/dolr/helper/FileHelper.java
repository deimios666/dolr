package ro.deimios.dolr.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.model.UploadedFile;
import ro.deimios.dolr.facade.FileFacadeLocal;

/**
 *
 * @author deimios
 */
public class FileHelper {

    //private static final int BUFFER_SIZE = 8096;
    private final FileFacadeLocal fileFacade = lookupFileFacadeLocal();

    public int saveFile(UploadedFile upFile) throws FileNotFoundException, IOException {
        String fileHash = CryptHelper.sha512(upFile);
        ro.deimios.dolr.entity.File dbFile = fileFacade.findByFileHash(fileHash);
        if (dbFile == null) {
            dbFile = new ro.deimios.dolr.entity.File();
            dbFile.setFilename(upFile.getFileName());
            dbFile.setFileType(upFile.getContentType());
            dbFile.setFileHash(fileHash);
            dbFile.setContents(upFile.getContents());
            fileFacade.create(dbFile);
        }
        return dbFile.getId();
    }
    
    /*
     public static File saveUploadedFile(InputStream inputStream, String fileName) throws FileNotFoundException, IOException {
     return saveUploadedFile(inputStream, fileName, true);
     }

     public static File saveUploadedFile(InputStream inputStream, String fileName, boolean skipExistingFile) throws FileNotFoundException, IOException {
     File outputFile = new File(fileName);
     if (!(outputFile.exists() && skipExistingFile)) {
     FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
     byte[] buffer = new byte[BUFFER_SIZE];
     int bulk;
     while (true) {
     bulk = inputStream.read(buffer);
     if (bulk < 0) {
     break;
     }
     fileOutputStream.write(buffer, 0, bulk);
     fileOutputStream.flush();
     }
     fileOutputStream.close();
     inputStream.close();
     }
     return outputFile;
     }
     */

    private FileFacadeLocal lookupFileFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (FileFacadeLocal) c.lookup("java:comp/env/ejb/FileFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
