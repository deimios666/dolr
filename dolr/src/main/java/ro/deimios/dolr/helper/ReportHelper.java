package ro.deimios.dolr.helper;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ro.deimios.dolr.datatype.xlCell;
import ro.deimios.dolr.datatype.xlCellComparator;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.entity.Unitate;

/**
 *
 * @author deimios
 */
public class ReportHelper {

    private static final Logger LOG = Logger.getLogger(ReportHelper.class.getName());

    public static Workbook createReport(boolean isFixedTable, String reportRange, int reportTypeId, boolean isReportingOnSubUnit, List<Transfer> lastResponseList) {
        LOG.log(Level.FINEST, "Starting report creation");
        //create workbook in memory
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Sheet1");
        LOG.log(Level.FINEST, "Sheet created");
        //if stat type is variable then we can only flatten
        if (!isFixedTable) {
            LOG.log(Level.FINEST, "Not a fixed table");
            //range refers to header , we need to scan for data UNDER this range
            //TODO implement variaable copy
        } else {
            LOG.log(Level.FINEST, "Report range: {0}", reportRange);
            //range refers to actual data
            List<xlCell> cellList = ExcelHelper.refToCellList(reportRange);
            //sort order is row then column
            Collections.sort(cellList, new xlCellComparator());
            //reportTypeId can be 0 (copy with flatten) or 1(sum)
            if (reportTypeId == 0) {
                //flatten and copy

                int currentRow = 1;

                Row createdRow = sheet.createRow(currentRow);
                LOG.log(Level.FINEST, "Response list size: {0}", lastResponseList.size());
                for (int i = 0; i < lastResponseList.size(); i++) {
                    InputStream fis;
                    try {
                        Transfer raspuns = lastResponseList.get(i);
                        //get the file hash
                        //String fileHash = raspuns.getAttachment().getFileHash();
                        //open file
                        //fis = new FileInputStream(filePrefix + fileHash);
                        fis = new ByteArrayInputStream(raspuns.getAttachment().getContents());
                        //open workbook
                        Workbook wb = WorkbookFactory.create(fis);
                        if (!isReportingOnSubUnit) {
                            //PJ
                            Sheet s = wb.getSheetAt(0);
                            Iterator<xlCell> cellWalker = cellList.iterator();
                            int currentCol = 2; //start from C, since A will have PJ sirues name and B will have PJ name
                            createdRow.createCell(0).setCellValue(raspuns.getSender().getSirues());
                            createdRow.createCell(1).setCellValue(raspuns.getSender().getNume());
                            while (cellWalker.hasNext()) {
                                xlCell c = cellWalker.next();
                                //copy data
                                Cell originCell = s.getRow(c.getRow()).getCell(c.getCol());
                                Cell createdCell = createdRow.createCell(currentCol);
                                switch (originCell.getCellType()) {
                                    case Cell.CELL_TYPE_STRING:
                                        createdCell.setCellValue(originCell.getRichStringCellValue());
                                        break;
                                    case Cell.CELL_TYPE_NUMERIC:
                                        createdCell.setCellValue(originCell.getNumericCellValue());
                                        break;
                                    case Cell.CELL_TYPE_BOOLEAN:
                                        createdCell.setCellValue(originCell.getBooleanCellValue());
                                        break;
                                    case Cell.CELL_TYPE_FORMULA:
                                        createdCell.setCellValue(originCell.getCellFormula());
                                        break;
                                }
                                currentCol++;
                            }
                            currentRow++;
                            createdRow = sheet.createRow(currentRow);
                        } else {
                            //AR
                            //loop over all sheets
                            //get ar list
                            List<Unitate> arList = raspuns.getSender().getUnitateList();
                            Iterator<Unitate> arWalker = arList.iterator();
                            while (arWalker.hasNext()) {
                                Unitate currentAr = arWalker.next();
                                String sirues = String.valueOf(currentAr.getSirues());
                                Sheet s = wb.getSheet(sirues);
                                Iterator<xlCell> cellWalker = cellList.iterator();
                                int currentCol = 4; //start from E
                                createdRow.createCell(0).setCellValue(raspuns.getSender().getSirues());
                                createdRow.createCell(1).setCellValue(raspuns.getSender().getNume());
                                createdRow.createCell(2).setCellValue(currentAr.getSirues());
                                createdRow.createCell(3).setCellValue(currentAr.getNume());
                                while (cellWalker.hasNext()) {
                                    xlCell c = cellWalker.next();
                                    //copy data
                                    Cell originCell = s.getRow(c.getRow()).getCell(c.getCol());
                                    Cell createdCell = createdRow.createCell(currentCol);
                                    switch (originCell.getCellType()) {
                                        case Cell.CELL_TYPE_STRING:
                                            createdCell.setCellValue(originCell.getRichStringCellValue());
                                            break;
                                        case Cell.CELL_TYPE_NUMERIC:
                                            createdCell.setCellValue(originCell.getNumericCellValue());
                                            break;
                                        case Cell.CELL_TYPE_BOOLEAN:
                                            createdCell.setCellValue(originCell.getBooleanCellValue());
                                            break;
                                        case Cell.CELL_TYPE_FORMULA:
                                            createdCell.setCellValue(originCell.getCellFormula());
                                            break;
                                    }
                                    currentCol++;
                                }
                                currentRow++;
                                createdRow = sheet.createRow(currentRow);
                            }
                        }
                        //close workbook reader
                        fis.close();
                        //}
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ExcelHelper.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException | InvalidFormatException ex) {
                        Logger.getLogger(ExcelHelper.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                //sum
            }
        }
        return workbook;
    }

}
