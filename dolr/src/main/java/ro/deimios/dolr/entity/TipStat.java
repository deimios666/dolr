/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "tip_stat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipStat.findAll", query = "SELECT t FROM TipStat t"),
    @NamedQuery(name = "TipStat.findById", query = "SELECT t FROM TipStat t WHERE t.id = :id"),
    @NamedQuery(name = "TipStat.findByNume", query = "SELECT t FROM TipStat t WHERE t.nume = :nume"),
    @NamedQuery(name = "TipStat.findByD", query = "SELECT t FROM TipStat t WHERE t.d = :d"),
    @NamedQuery(name = "TipStat.findByAr", query = "SELECT t FROM TipStat t WHERE t.ar = :ar"),
    @NamedQuery(name = "TipStat.findByVariable", query = "SELECT t FROM TipStat t WHERE t.variable = :variable")})
public class TipStat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    //@NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "nume")
    private String nume;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is2d")
    private boolean d;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AR")
    private boolean ar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "variable")
    private boolean variable;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipStat")
    private List<Registru> registruList;

    public TipStat() {
    }

    public TipStat(Integer id) {
        this.id = id;
    }

    public TipStat(Integer id, String nume, boolean d, boolean ar, boolean variable) {
        this.id = id;
        this.nume = nume;
        this.d = d;
        this.ar = ar;
        this.variable = variable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public boolean getD() {
        return d;
    }

    public void setD(boolean d) {
        this.d = d;
    }

    public boolean getAr() {
        return ar;
    }

    public void setAr(boolean ar) {
        this.ar = ar;
    }

    public boolean getVariable() {
        return variable;
    }

    public void setVariable(boolean variable) {
        this.variable = variable;
    }

    @XmlTransient
    public List<Registru> getRegistruList() {
        return registruList;
    }

    public void setRegistruList(List<Registru> registruList) {
        this.registruList = registruList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipStat)) {
            return false;
        }
        TipStat other = (TipStat) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dolr.entity.TipStat[ id=" + id + " ]";
    }
    
}
