package ro.deimios.dolr.converter;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.dolr.entity.TipStat;
import ro.deimios.dolr.facade.TipStatFacadeLocal;

/**
 *
 * @author Deimios
 */
@FacesConverter(forClass = ro.deimios.dolr.entity.TipStat.class, value = "tipStatConverter")
public class TipStatConverter implements Converter {

    TipStatFacadeLocal tipStatFacade = lookupTipStatFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        int tipStatId = Integer.valueOf(value);
        return tipStatFacade.find(tipStatId);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        TipStat tipStat = (TipStat) value;
        return "" + tipStat.getId();
    }

    private TipStatFacadeLocal lookupTipStatFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (TipStatFacadeLocal) c.lookup("java:comp/env/ejb/TipStatFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
