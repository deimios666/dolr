package ro.deimios.dolr.datatype;

/**
 *
 * @author Deimios
 */
public class UnitateRaspuns {

    public static int STATUS_UNSENT = 0;
    public static int STATUS_SENT_BEFORE_DEADLINE = 1;
    public static int STATUS_SENT_AFTER_DEADLINE = 2;
    private int status;
    private String numeUnitate;

    public UnitateRaspuns() {
    }

    public UnitateRaspuns(int status, String numeUnitate) {
        this.status = status;
        this.numeUnitate = numeUnitate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNumeUnitate() {
        return numeUnitate;
    }

    public void setNumeUnitate(String numeUnitate) {
        this.numeUnitate = numeUnitate;
    }
}
