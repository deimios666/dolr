package ro.deimios.dolr.bean;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.UploadedFile;
import ro.deimios.dolr.datatype.UnitateRaspuns;
import ro.deimios.dolr.entity.*;
import ro.deimios.dolr.facade.*;
import ro.deimios.dolr.helper.BeanHelper;
import ro.deimios.dolr.helper.CryptHelper;
import ro.deimios.dolr.helper.FileHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class SendBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(SendBean.class.getName());
    @EJB
    private TipTransferFacadeLocal tipTransferFacade;
    @EJB
    private TransferFacadeLocal transferFacade;
    @EJB
    private RegistruFacadeLocal registruFacade;
    @EJB
    private FileFacadeLocal fileFacade;
    @EJB
    private UnitateFacadeLocal unitateFacade;
    private String titlu;
    private String descriere;
    private Date termen;
    private int fileId;
    private DualListModel<Unitate> unitati;
    private TipStat tipStat;
    private FileHelper fileHelper;

    /**
     * Creates a new instance of SendBean
     */
    public SendBean() {
    }

    @PostConstruct
    public void loadData() {
        List<Unitate> source = unitateFacade.findAllNonISJPJ();
        List<Unitate> target = new ArrayList<>();
        unitati = new DualListModel<>(source, target);
        fileHelper = new FileHelper();
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            fileId = -1;
            String fileName = event.getFile().getFileName();
            fileId = fileHelper.saveFile(event.getFile());
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Fișier Încărcat", "Fișierul " + fileName + " s-a încărcat cu succes");
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        } catch (IOException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul nu s-a încărcat");
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        }
    }

    public void doSendStat() {
        try {
            //declare common vars
            Unitate target;
            TipTransfer tipTransfer = tipTransferFacade.find(1); //set transfer type Adresă
            UserBean user = (UserBean) BeanHelper.getBean("userBean");
            Date currentDate = new Date();

            //create registru
            Registru registru = new Registru();
            registru.setData(currentDate);
            registru.setShortname(titlu);
            registru.setDescription(descriere);
            registru.setTermen(termen);
            registru.setTipStat(tipStat);
            registruFacade.create(registru);

            //save file
            File fileToDb = fileFacade.find(fileId);

            //create transfers for each target
            Iterator<Unitate> targetWalker = unitati.getTarget().iterator();
            while (targetWalker.hasNext()) {
                target = targetWalker.next();
                Transfer dbTransfer = new Transfer();
                dbTransfer.setTipTransfer(tipTransfer);
                dbTransfer.setSender(user.user.getUnitate());
                dbTransfer.setReciever(target);
                dbTransfer.setAttachment(fileToDb);
                dbTransfer.setRegistru(registru);
                dbTransfer.setSentOn(currentDate);
                dbTransfer.setRaspStatus(UnitateRaspuns.STATUS_UNSENT);
                transferFacade.create(dbTransfer);
            }

            //Reset data
            titlu = null;
            descriere = null;
            termen = null;
            fileId = -1;

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trimis cu succes", "Nr. " + registru.getNr());
            FacesContext.getCurrentInstance().addMessage("", msg);

        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare trimitere", ex.toString());
            FacesContext.getCurrentInstance().addMessage("", msg);
        }
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public Date getTermen() {
        return termen;
    }

    public void setTermen(Date termen) {
        this.termen = termen;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public DualListModel<Unitate> getUnitati() {
        return unitati;
    }

    public void setUnitati(DualListModel<Unitate> unitati) {
        this.unitati = unitati;
    }

    public TipStat getTipStat() {
        return tipStat;
    }

    public void setTipStat(TipStat tipStat) {
        this.tipStat = tipStat;
    }
}
