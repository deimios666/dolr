package ro.deimios.dolr.bean;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.dolr.entity.TipStat;
import ro.deimios.dolr.facade.TipStatFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@RequestScoped
public class tipStatListerBean {

    private List<TipStat> tipStatList;
    @EJB
    private TipStatFacadeLocal tipStatFacade;

    /**
     * Creates a new instance of tipStatListerBean
     */
    public tipStatListerBean() {
    }

    @PostConstruct
    public void loadData() {
        tipStatList = tipStatFacade.findAll();
    }

    public List<TipStat> getTipStatList() {
        return tipStatList;
    }
}
