package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class UnitateRegistruListerBean implements Serializable {

    @EJB
    private TransferFacadeLocal transferFacade;
    List<Transfer> transferList;

    /**
     * Creates a new instance of UnitateRegistruListerBean
     */
    public UnitateRegistruListerBean() {
    }

    @PostConstruct
    public void loadData() {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        transferList = transferFacade.findAllForUnitate(userBean.user.getUnitate().getSirues(), 1);
        Collections.reverse(transferList);
    }

    public List<Transfer> getTransferList() {
        return transferList;
    }

    public TransferFacadeLocal getTransferFacade() {
        return transferFacade;
    }

    public void setTransferFacade(TransferFacadeLocal transferFacade) {
        this.transferFacade = transferFacade;
    }
}
