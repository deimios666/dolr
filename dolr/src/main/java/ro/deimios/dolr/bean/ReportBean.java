package ro.deimios.dolr.bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class ReportBean implements Serializable{

    private String reportRange;

    /**
     * Creates a new instance of ReportBean
     */
    public ReportBean() {
    }

    public String getReportRange() {
        return reportRange;
    }

    public void setReportRange(String reportRange) {
        this.reportRange = reportRange;
    }
}
