package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.PieChartModel;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.facade.TransferFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class ResponseListerBean implements Serializable {

    @EJB
    private TransferFacadeLocal transferFacade;
    int selectedRegistruId;
    private List<Transfer> raspuns;
    private List<Transfer> filteredRaspuns;
    private PieChartModel pieModel;

    /**
     * Creates a new instance of ResponseListerBean
     */
    public ResponseListerBean() {
    }

    public void loadTransferData() {
        int unsent = 0;
        int expired = 0;
        int sent = 0;

        List<Transfer> transferList = transferFacade.findAllByRegistruByTip(selectedRegistruId, 1);
        for (int i = 0; i < transferList.size(); i++) {
            Transfer currentTransfer = transferList.get(i);
            int raspStatus;
            try {
                raspStatus = currentTransfer.getRaspStatus();
            } catch (NullPointerException npe) {
                raspStatus = 0;
            }
            if (raspStatus == 0) {
                unsent++;
            } else if (raspStatus == 1) {
                sent++;
            } else {
                expired++;
            }
            //}
        }
        pieModel = new PieChartModel();
        pieModel.set("Fără răspuns", unsent);
        pieModel.set("Ieșit din termen", expired);
        pieModel.set("Răspuns la timp", sent);

        raspuns = transferList;

    }

    public int getSelectedRegistruId() {
        return selectedRegistruId;
    }

    public void setSelectedRegistruId(int selectedRegistruId) {
        this.selectedRegistruId = selectedRegistruId;
        loadTransferData();
    }

    public List<Transfer> getRaspuns() {
        return raspuns;
    }

    public List<Transfer> getResponseList(int transferId) {
        return transferFacade.findResponses(transferId);
    }

    public void setRaspuns(List<Transfer> raspuns) {
        this.raspuns = raspuns;
    }

    public PieChartModel getPieModel() {
        return pieModel;
    }

    public List<Transfer> getFilteredRaspuns() {
        return filteredRaspuns;
    }

    public void setFilteredRaspuns(List<Transfer> filteredRaspuns) {
        this.filteredRaspuns = filteredRaspuns;
    }
}
