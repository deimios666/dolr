package ro.deimios.dolr.bean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.dolr.entity.TipUnitate;
import ro.deimios.dolr.entity.TipUser;
import ro.deimios.dolr.entity.Unitate;
import ro.deimios.dolr.entity.User;
import ro.deimios.dolr.facade.TipUnitateFacadeLocal;
import ro.deimios.dolr.facade.TipUserFacadeLocal;
import ro.deimios.dolr.facade.UnitateFacadeLocal;
import ro.deimios.dolr.facade.UserFacadeLocal;
import ro.deimios.dolr.helper.CryptHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class InitBean {
    
    @EJB
    private TipUserFacadeLocal tipUserFacade;
    
    @EJB
    private TipUnitateFacadeLocal tipUnitateFacade;
    
    @EJB
    private UnitateFacadeLocal unitateFacade;
    
    @EJB
    private UserFacadeLocal userFacade;

    /**
     * Creates a new instance of InitBean
     */
    public InitBean() {
    }
    
    public void loadInitialData() {
        //check if db is empty
        int userCount = userFacade.userCount();
        if (userCount == 0) {
            //load default nomenclators

            //tip unitate
            String[] numeTipUnitati = {"Local", "Județean", "Conexe", "Inspectorat Școlar", "Ministerul Educației Naționale"};
            for (String numeTipUnitate : numeTipUnitati) {
                TipUnitate tipUnitate = new TipUnitate();
                tipUnitate.setNume(numeTipUnitate);
                tipUnitateFacade.create(tipUnitate);
            }

            //tip user
            TipUser tipUserUnitate = new TipUser("Unitate", true, true, false, false, false, false, false);
            tipUserFacade.create(tipUserUnitate);
            TipUser tipUserISJ = new TipUser("Inspectorat", true, true, true, false, true, true, false);
            tipUserFacade.create(tipUserISJ);
            TipUser tipUserMEN = new TipUser("Minister", false, true, true, true, true, true, true);
            tipUserFacade.create(tipUserMEN);

            //unitate
            Unitate unitateMEN = new Unitate();
            unitateMEN.setNume("Ministerul Educației Naționale");
            unitateMEN.setTipUnitate(tipUnitateFacade.findByNume("Ministerul Educației Naționale"));
            unitateMEN.setSuperior(unitateMEN);
            unitateFacade.create(unitateMEN);

            //create default admin user
            User userAdmin = new User();
            userAdmin.setUsername("admin");
            userAdmin.setPassword(CryptHelper.sha512("admin"));
            userAdmin.setUnitate(unitateMEN);
            userAdmin.setTipUser(tipUserFacade.findByNume("Minister"));
            userFacade.create(userAdmin);
        }
    }
    
}
