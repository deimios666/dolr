package ro.deimios.dolr.bean;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import ro.deimios.dolr.datatype.UnitateRaspuns;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class RegistruListerBean implements Serializable {

    @EJB
    private TransferFacadeLocal transferFacade;
    @EJB
    private RegistruFacadeLocal registruFacade;
    private List<Registru> registruList;
    private SelectItem[] responseTypes;

    /**
     * Creates a new instance of RegistruListerBean
     */
    public RegistruListerBean() {
        responseTypes = new SelectItem[4];
        responseTypes[0] = new SelectItem("", "Toate");
        responseTypes[1] = new SelectItem(UnitateRaspuns.STATUS_UNSENT, "Fără");
        responseTypes[2] = new SelectItem(UnitateRaspuns.STATUS_SENT_AFTER_DEADLINE, "Ieșit din termen");
        responseTypes[3] = new SelectItem(UnitateRaspuns.STATUS_SENT_BEFORE_DEADLINE, "La Termen");
    }

    @PostConstruct
    public void loadData() {
        registruList = registruFacade.findAll();
        Collections.reverse(registruList);
    }

    public List<Registru> getRegistruList() {
        return this.registruList;
    }

    public void setRegistruList(List<Registru> registruList) {
        this.registruList = registruList;
    }

    public SelectItem[] getResponseTypes() {
        if (responseTypes == null) {
            responseTypes = new SelectItem[4];
            responseTypes[0] = new SelectItem("", "Toate");
            responseTypes[1] = new SelectItem(UnitateRaspuns.STATUS_UNSENT, "Fără");
            responseTypes[2] = new SelectItem(UnitateRaspuns.STATUS_SENT_AFTER_DEADLINE, "Ieșit din termen");
            responseTypes[3] = new SelectItem(UnitateRaspuns.STATUS_SENT_BEFORE_DEADLINE, "La Termen");
        }
        return responseTypes;
    }

    public void setResponseTypes(SelectItem[] responseTypes) {
        this.responseTypes = responseTypes;
    }

    public TransferFacadeLocal getTransferFacade() {
        return transferFacade;
    }

    public void setTransferFacade(TransferFacadeLocal transferFacade) {
        this.transferFacade = transferFacade;
    }
}
