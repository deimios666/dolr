package ro.deimios.dolr.bean;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import ro.deimios.dolr.datatype.UnitateRaspuns;
import ro.deimios.dolr.entity.File;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.facade.FileFacadeLocal;
import ro.deimios.dolr.facade.TipTransferFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;
import ro.deimios.dolr.helper.CryptHelper;
import ro.deimios.dolr.helper.FileHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@RequestScoped
public class ResponseBean implements Serializable {

    @EJB
    private TipTransferFacadeLocal tipTransferFacade;
    @EJB
    private FileFacadeLocal fileFacade;
    @EJB
    private TransferFacadeLocal transferFacade;
    //private String fileHash;
    //private File file;
    private String fileName;
    private final FileHelper fileHelper;

    /**
     * Creates a new instance of ResponseBean
     */
    public ResponseBean() {
        fileHelper = new FileHelper();
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            //Uploaded file data
            InputStream is = event.getFile().getInputstream();
            HSSFWorkbook workbook = new HSSFWorkbook(is);
            Sheet idSheet = workbook.getSheet("ID");
            if (idSheet == null) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Macheta nu este destinată unității Dvs. Folosiți macheta descarcată de pe site!");
                FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
                return;
            }

            int transferId = (int) idSheet.getRow(1).getCell(0).getNumericCellValue();
            String checkSum = idSheet.getRow(0).getCell(0).getRichStringCellValue().getString();
            is.close();

            //Server data
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            int sirues = userBean.user.getUnitate().getSirues();
            Transfer transfer = transferFacade.find(transferId);
            int transferSirues = transfer.getReciever().getSirues();
            if (sirues != transferSirues) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Macheta nu este destinată unității Dvs. Folosiți macheta descarcată de pe site!");
                FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
                return;
            }
            String dbFileHash = transfer.getAttachment().getFileHash();
            String serverHash = CryptHelper.sha512(dbFileHash + transferSirues);
            if (!serverHash.equals(checkSum)) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Macheta nu este destinată unității Dvs. Folosiți macheta descarcată de pe site!");
                FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
                return;
            }
            fileName = event.getFile().getFileName();
            int fileId = fileHelper.saveFile(event.getFile());
            //save file
            ro.deimios.dolr.entity.File fileToDb = fileFacade.find(fileId);

            //set status
            Date now = new Date();
            Date termen = transfer.getRegistru().getTermen();
            if (now.after(termen)) {
                transfer.setRaspStatus(UnitateRaspuns.STATUS_SENT_AFTER_DEADLINE);
            } else {
                transfer.setRaspStatus(UnitateRaspuns.STATUS_SENT_BEFORE_DEADLINE);
            }

            transferFacade.edit(transfer);

            //create transfer
            Transfer transferToDb = new Transfer();
            transferToDb.setAttachment(fileToDb);
            transferToDb.setSender(userBean.user.getUnitate());
            transferToDb.setReciever(transfer.getSender());
            transferToDb.setRaspunsLa(transfer);
            transferToDb.setSentOn(now);
            transferToDb.setTipTransfer(tipTransferFacade.find(2));
            transferToDb.setRegistru(transfer.getRegistru());
            transferFacade.create(transferToDb);
            if (transfer.getRaspStatus() == UnitateRaspuns.STATUS_SENT_AFTER_DEADLINE) {
                FacesMessage msgDupaTermen = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Termen Expirat", "Ați răspuns la solicitare după termenul stabilit, vă rugăm să respectați termenele în viitor!");
                FacesContext.getCurrentInstance().addMessage("fileUploader", msgDupaTermen);
            } else {
                FacesMessage msgInainteTermen = new FacesMessage(FacesMessage.SEVERITY_INFO, "Încadrare în termen", "Vă mulțumim că v-ați încadrat în termenul prestabilit.");
                FacesContext.getCurrentInstance().addMessage("fileUploader", msgInainteTermen);
            }
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Fișier Încărcat", "Fișierul '" + fileName + "' s-a încărcat cu succes ca răspuns la solicitarea: '" + transfer.getRegistru().getShortname() + "'. NR TRANSFER: " + transferToDb.getId());
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        } catch (IOException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul nu s-a încărcat");
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        } catch (org.apache.poi.poifs.filesystem.OfficeXmlFileException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul încărcat este în format 2007! Se acceptă doar format 2003!");
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        } catch (org.apache.poi.hssf.OldExcelFormatException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Încărcare", "Fișierul încărcat este în format excel 95! Se acceptă doar format 2003!");
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        } catch (NumberFormatException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Eroare Procesare", "Fișierul nu conține o machetă validă!");
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        } catch (NullPointerException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Procesare", "Folosiți macheta descarcată de pe site!");
            FacesContext.getCurrentInstance().addMessage("fileUploader", msg);
        }

    }
}
