package ro.deimios.dolr.bean;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import ro.deimios.dolr.entity.User;
import ro.deimios.dolr.facade.UserFacadeLocal;
import ro.deimios.dolr.helper.CryptHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable {

    @EJB
    private UserFacadeLocal userFacade;
    boolean isLoggedIn = false;
    boolean MEN = false;
    boolean ISJ = false;
    boolean Unitate = false;
    boolean needPasswordChange = false;
    String username;
    String oldpassword;
    String password;
    String password2;
    User user;

    /**
     * Creates a new instance of UserBean
     */
    public UserBean() {
    }

    public void doLogin() {
        User dbUser = userFacade.findByUsername(username);
        if (dbUser != null) {
            user = dbUser;
            if (dbUser.getPassword().equals(password)) {
                isLoggedIn = true;
                MEN = user.getTipUser().getAdminTara();
                if (!MEN) {
                    ISJ = user.getTipUser().getAdminJudet();
                    Unitate = !ISJ;
                } else {
                    ISJ = false;
                    Unitate = false;
                }
                needPasswordChange = user.getPassword().equals(CryptHelper.sha512(user.getUsername()));
                password = null;
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Autentificare Reușită", "Autentificat ca operator " + user.getUnitate().getNume());
                FacesContext.getCurrentInstance().addMessage("loginMessages", msg);
            } else {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Autentificare", "Nume utilizator sau parolă incorectă");
                FacesContext.getCurrentInstance().addMessage("loginMessages", msg);
            }
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Autentificare", "Nume utilizator sau parolă incorectă");
            FacesContext.getCurrentInstance().addMessage("loginMessages", msg);
        }
    }

    public void doPasswordChange() {
        if (password.equals(password2) && user.getPassword().equals(oldpassword)) {
            user.setPassword(password);
            userFacade.edit(user);
            needPasswordChange = user.getPassword().equals(CryptHelper.sha512(user.getUsername()));
            oldpassword = null;
            password = null;
            password2 = null;
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Schimbare Parolă Reușită", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            System.out.println("Not changing password");
        }
    }

    public void doLogout() {
        isLoggedIn = false;
        user = null;
        MEN = false;
        ISJ = false;
        Unitate = false;
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        request.getSession(true).invalidate();
    }

    public boolean isIsLoggedIn() {
        return isLoggedIn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = CryptHelper.sha512(password);
    }

    public boolean isISJ() {
        return ISJ;
    }

    public boolean isMEN() {
        return MEN;
    }

    public boolean isUnitate() {
        return Unitate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User getUser() {
        return user;
    }

    public String getRSS() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String scheme = request.getScheme();             // http
        String serverName = request.getServerName();     // hostname.com
        int serverPort = request.getServerPort();        // 80
        String contextPath = request.getContextPath();   // /mywebapp
        String baseUrl = scheme + "://" + serverName + ":" + serverPort + contextPath;
        String params = "?uid=" + user.getId() + "&hash=" + CryptHelper.sha512(user.getUsername() + user.getPassword());
        return baseUrl + "/rss.xml" + params;
    }

    public boolean isNeedPasswordChange() {
        return needPasswordChange;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = CryptHelper.sha512(password2);
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = CryptHelper.sha512(oldpassword);
    }
}
