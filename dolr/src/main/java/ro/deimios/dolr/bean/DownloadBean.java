package ro.deimios.dolr.bean;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import ro.deimios.dolr.entity.Registru;
import ro.deimios.dolr.entity.TipStat;
import ro.deimios.dolr.entity.Transfer;
import ro.deimios.dolr.entity.Unitate;
import ro.deimios.dolr.entity.User;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;
import ro.deimios.dolr.helper.CryptHelper;
import ro.deimios.dolr.helper.ExcelHelper;
import ro.deimios.dolr.helper.ReportHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class DownloadBean {

    @EJB
    private RegistruFacadeLocal registruFacade;

    @EJB
    private TransferFacadeLocal transferFacade;

    private static final Logger LOG = Logger.getLogger(DownloadBean.class.getName());
    private static final int BUFFER_SIZE = 8096;

    private int tid;
    private int rid;
    private String reportRange;

    /**
     * Creates a new instance of DownloadBean
     */
    public DownloadBean() {
    }

    public void downloadFile() {
        try {
            Transfer transfer = transferFacade.find(tid);
            String templateId = transfer.getAttachment().getFileHash();
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            User user = userBean.getUser();
            String unitateNume = user.getUnitate().getNume();
            String unitateSirues = "" + user.getUnitate().getSirues();
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"macheta_2013_" + tid + ".xls\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

            InputStream templateInputStream;
            Workbook template;
            Sheet mainSheet;
            Sheet idSheet;
            templateInputStream = new ByteArrayInputStream(transfer.getAttachment().getContents());
            if (templateInputStream == null) {
                throw new FileNotFoundException("Template file not found!");
            }
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);
            boolean wasProtected = mainSheet.getProtect();
            if (wasProtected) {
                //unprotect sheet
                mainSheet.protectSheet(null);
            }
            TipStat tipStat = transfer.getRegistru().getTipStat();

            List<Unitate> arList;

            if (tipStat.getAr()) {
                arList = user.getUnitate().getUnitateList();
                template.setSheetName(0, String.valueOf(arList.get(0).getSirues()));
                try {
                    mainSheet.getRow(0).getCell(1).setCellValue(arList.get(0).getNume());
                } catch (NullPointerException ex) {
                    mainSheet.getRow(0).createCell(1).setCellValue(arList.get(0).getNume());
                }

                for (int i = 0; i < arList.size(); i++) {
                    if (i > 0) {
                        Sheet newCopy = template.cloneSheet(0);
                        int copyIndex = template.getSheetIndex(newCopy);
                        String sirues = String.valueOf(arList.get(i).getSirues());
                        template.setSheetName(copyIndex, sirues);
                        template.setSheetOrder(sirues, i);
                        try {
                            newCopy.getRow(0).getCell(1).setCellValue(arList.get(i).getNume());
                        } catch (NullPointerException ex) {
                            newCopy.getRow(0).createCell(1).setCellValue(arList.get(i).getNume());
                        }
                        if (wasProtected) {
                            newCopy.protectSheet("");
                        }
                    }
                }
            } else {
                try {
                    mainSheet.getRow(0).getCell(1).setCellValue(unitateNume);
                } catch (NullPointerException ex) {
                    mainSheet.getRow(0).createCell(1).setCellValue(unitateNume);
                }
            }
            if (wasProtected) {
                mainSheet.protectSheet("");
            }

            //recalcualte formulas
            FormulaEvaluator evaluator = template.getCreationHelper().createFormulaEvaluator();
            for (int sheetNum = 0; sheetNum < template.getNumberOfSheets(); sheetNum++) {
                Sheet sheet = template.getSheetAt(sheetNum);
                for (Row r : sheet) {
                    for (Cell c : r) {
                        if (c.getCellType() == Cell.CELL_TYPE_FORMULA) {
                            evaluator.evaluateFormulaCell(c);
                        }
                    }
                }
            }

            //get old ID sheet if it exists
            idSheet = template.getSheet("ID");
            if (idSheet == null) {
                idSheet = template.createSheet("ID");
            }
            template.setSheetOrder("ID", template.getNumberOfSheets() - 1);
            idSheet.createRow(0).createCell(0).setCellValue(CryptHelper.sha512(templateId + unitateSirues));
            idSheet.createRow(1).createCell(0).setCellValue(tid);
            //set sheet very hidden
            template.setSheetHidden(template.getNumberOfSheets() - 1, 2);
            OutputStream out = ec.getResponseOutputStream();
            template.write(out);
            out.flush();
            out.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
        } catch (InvalidFormatException | IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } finally {
            //out.close();
        }

    }

    public void downloadReport() {
        try {
            LOG.log(Level.FINEST, "Started report");
            int registruId = rid;
            int reportTypeId = 0;

            LOG.log(Level.FINEST, "Reportrange: {0}", reportRange);
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            User user = userBean.getUser();
            //if no user set or unitate is not ISJ abort
            if (user == null || user.getUnitate().getTipUnitate().getId() != 4) {
                return;
            }
            LOG.log(Level.FINEST, "Got user: {0}", user.getUsername());
            Registru registru = registruFacade.find(registruId);
            LOG.log(Level.FINEST, "Found registru: {0}", registru.getNr());
            boolean isVariable = registru.getTipStat().getVariable();
            //boolean isVariable = false;
            boolean is2D = registru.getTipStat().getD();
            boolean isAr = registru.getTipStat().getAr();

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"centralizator_" + registruId + ".xlsx\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
            List<Transfer> responseList = new ArrayList<>();
            List<Transfer> transferList = transferFacade.findAllByRegistruByTip(registruId, 1);
            LOG.log(Level.FINEST, "Transferlist size: {0}", transferList.size());

            for (int i = 0; i < transferList.size(); i++) {
                List<Transfer> raspList = transferFacade.findResponses(transferList.get(i).getId());
                //check for responses
                if (raspList == null || raspList.isEmpty()) {
                    //no responses, ignore
                    LOG.log(Level.FINEST, "Transfer {0} has no responses", i);
                } else {
                    LOG.log(Level.FINEST, "Transfer {0} has {1} responses", new Object[]{i, raspList.size()});
                    responseList.add(raspList.get(raspList.size() - 1));
                }
            }
            Workbook workbook = ReportHelper.createReport(!isVariable, reportRange, reportTypeId, isAr, responseList);

            try (OutputStream out = ec.getResponseOutputStream()) {
                workbook.write(out);
                out.flush();
                out.close();

            }
            fc.responseComplete();
        } catch (IOException ex) {
        }

    }

    public void downloadOneFile() {
        try {
            Transfer transfer = transferFacade.find(tid);
            //UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            //User user = userBean.getUser();
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ro.deimios.dolr.entity.File file = transfer.getAttachment();
            String fileName = file.getFilename();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            if (fileName.endsWith("xlsx")) {
                ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
                ec.setResponseHeader("Content-Disposition", "attachment; filename=\"file_" + tid + ".xlsx\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
            } else if (fileName.endsWith("xls")) {
                ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
                ec.setResponseHeader("Content-Disposition", "attachment; filename=\"file_" + tid + ".xls\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
            }
            int length;
            InputStream inputStream = new ByteArrayInputStream(file.getContents());
            OutputStream outputStream = ec.getResponseOutputStream();
            byte[] buffer = new byte[BUFFER_SIZE];
            while (((length = inputStream.read(buffer)) != -1)) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
        } catch (IOException e) {
            LOG.severe(e.toString());
        }
    }

    public void downloadZip() {
        try {

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
            ec.setResponseContentType("application/zip"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"macheta_2013_" + tid + ".xls\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

            OutputStream out = ec.getResponseOutputStream();

            int registruId = rid;

            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            User user = userBean.getUser();

            //if unitate is not ISJ abort
            if (user.getUnitate().getTipUnitate().getId() != 4) {
                return;
            }
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"raspunsuri_2013_" + registruId + ".zip\"");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);
            byte buffer[] = new byte[BUFFER_SIZE];

            //get transfers for the selected registru
            List<Transfer> transferList = transferFacade.findAllByRegistruByTip(registruId, 1);
            //get the hash for the file for the last response transfer
            for (int i = 0; i < transferList.size(); i++) {
                List<Transfer> raspList = transferFacade.findResponses(transferList.get(i).getId());
                //check for responses
                if (raspList == null || raspList.isEmpty()) {
                    //no responses, ignore
                } else {
                    //get the latest transfer
                    Transfer raspuns = raspList.get(raspList.size() - 1);
                    //get the cleaned name of unitate
                    String unitateNume = raspuns.getSender().getNume().replaceAll("[\\\\/:\"*?<>|]+", "");
                    try (InputStream fis = new ByteArrayInputStream(raspuns.getAttachment().getContents()); BufferedInputStream bis = new BufferedInputStream(fis)) {

                        String fileExtension = ExcelHelper.getExt(raspuns.getAttachment().getFilename());
                        zos.putNextEntry(new ZipEntry(unitateNume + "." + fileExtension));

                        int bytesRead;
                        while ((bytesRead = bis.read(buffer)) != -1) {
                            zos.write(buffer, 0, bytesRead);
                        }
                        zos.closeEntry();
                    }

                }
            }
            zos.flush();
            baos.flush();
            zos.close();
            baos.close();
            out.write(baos.toByteArray());
            out.flush();
            out.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
        } catch (IOException e) {
            LOG.severe(e.toString());
        }
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public String getReportRange() {
        return reportRange;
    }

    public void setReportRange(String reportRange) {
        this.reportRange = reportRange;
    }

}
