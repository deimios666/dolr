package ro.deimios.dolr.bean;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import ro.deimios.dolr.facade.RegistruFacadeLocal;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.helper.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class DeleteBean {

    @EJB
    private RegistruFacadeLocal registruFacade;
    @EJB
    private TransferFacadeLocal transferFacade;
    private String nr;

    /**
     * Creates a new instance of DeleteBean
     */
    public DeleteBean() {
    }

    public void doDelete() {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        if (userBean.ISJ) {
            try {
                int registruNr = Integer.valueOf(nr);
                transferFacade.deleteByRegistruByTip(registruNr);
                registruFacade.deleteByNr(registruNr);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Șters cu succes", "Nr. " + nr);
                FacesContext.getCurrentInstance().addMessage("", msg);
            } catch (Exception ex) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare ștergere", ex.toString());
                FacesContext.getCurrentInstance().addMessage("", msg);
            }
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare ștergere", "Nu sunteți autorizat pentru ștergere");
            FacesContext.getCurrentInstance().addMessage("", msg);
        }
    }

    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }
}
