package ro.deimios.dolr.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import ro.deimios.dolr.bean.UserBean;
import ro.deimios.dolr.entity.TipStat;
import ro.deimios.dolr.entity.Unitate;
import ro.deimios.dolr.entity.User;
import ro.deimios.dolr.facade.TransferFacadeLocal;
import ro.deimios.dolr.facade.UserFacadeLocal;
import ro.deimios.dolr.helper.CryptHelper;

/**
 *
 * @author Deimios
 */
@WebServlet(name = "getFile", urlPatterns = {"/getFile"})
public class getFile extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(getFile.class.getName());

    @EJB
    private TransferFacadeLocal transferFacade;
    @EJB
    private UserFacadeLocal userFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String templateId = request.getParameter("fid");
            int transferId = 0;
            try {
                transferId = Integer.valueOf(request.getParameter("tid"));
            } catch (NumberFormatException ignored) {
            }
            if (transferId < 1) {
                return;
            }
            UserBean userBean = null;
            userBean = (UserBean) request.getSession().getAttribute("userBean");
            User user = null;
            String hash;
            int uid;
            try {
                user = userBean.getUser();
            } catch (NullPointerException ignored) {
            }
            if (user == null) {

                hash = request.getParameter("hash");
                if (hash.length() != 128 || !hash.matches("[0-9a-f]{128}")) {
                    return;
                } else {

                    uid = Integer.valueOf(request.getParameter("uid"));
                    if (uid < 1) {
                        return;
                    }
                    user = userFacade.find(uid);
                    if (user != null) {
                        String dbHash = CryptHelper.sha512(user.getUsername() + user.getPassword());
                        if (!dbHash.equals(hash)) {
                            return;
                        }
                    }
                }
            } else {
                user = userBean.getUser();
            }

            String unitateNume = user.getUnitate().getNume();
            String unitateSirues = "" + user.getUnitate().getSirues();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=macheta_2013_" + transferId + ".xls");
            ServletContext context = getServletContext();
            InputStream templateInputStream;
            Workbook template;
            Sheet mainSheet;
            Sheet idSheet;
            templateInputStream = context.getResourceAsStream("/WEB-INF/upload/" + templateId);
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);
            boolean wasProtected = mainSheet.getProtect();
            if (wasProtected) {
                //unprotect sheet
                mainSheet.protectSheet(null);
            }
            TipStat tipStat = transferFacade.find(transferId).getRegistru().getTipStat();

            List<Unitate> arList;

            if (tipStat.getAr()) {
                arList = user.getUnitate().getUnitateList();
                template.setSheetName(0, String.valueOf(arList.get(0).getSirues()));
                try {
                    mainSheet.getRow(0).getCell(1).setCellValue(arList.get(0).getNume());
                } catch (NullPointerException ex) {
                    mainSheet.getRow(0).createCell(1).setCellValue(arList.get(0).getNume());
                }

                for (int i = 0; i < arList.size(); i++) {
                    if (i > 0) {
                        Sheet newCopy = template.cloneSheet(0);
                        int copyIndex = template.getSheetIndex(newCopy);
                        String sirues = String.valueOf(arList.get(i).getSirues());
                        template.setSheetName(copyIndex, sirues);
                        template.setSheetOrder(sirues, i);
                        try {
                            newCopy.getRow(0).getCell(1).setCellValue(arList.get(i).getNume());
                        } catch (NullPointerException ex) {
                            newCopy.getRow(0).createCell(1).setCellValue(arList.get(i).getNume());
                        }
                        if (wasProtected) {
                            newCopy.protectSheet("");
                        }
                    }
                }
            } else {
                try {
                    mainSheet.getRow(0).getCell(1).setCellValue(unitateNume);
                } catch (NullPointerException ex) {
                    mainSheet.getRow(0).createCell(1).setCellValue(unitateNume);
                }
            }
            if (wasProtected) {
                mainSheet.protectSheet("");
            }

            //recalcualte formulas
            FormulaEvaluator evaluator = template.getCreationHelper().createFormulaEvaluator();
            for (int sheetNum = 0; sheetNum < template.getNumberOfSheets(); sheetNum++) {
                Sheet sheet = template.getSheetAt(sheetNum);
                for (Row r : sheet) {
                    for (Cell c : r) {
                        if (c.getCellType() == Cell.CELL_TYPE_FORMULA) {
                            evaluator.evaluateFormulaCell(c);
                        }
                    }
                }
            }

            //delete old ID sheet if it exists
            try {
                int idSheetIndex = -1;
                template.getSheetIndex("ID");
                template.removeSheetAt(idSheetIndex);
            } catch (Exception ex) {
            }

            //create ID sheet
            idSheet = template.createSheet("ID");
            template.setSheetOrder("ID", template.getNumberOfSheets() - 1);
            idSheet.createRow(0).createCell(0).setCellValue(CryptHelper.sha512(templateId + unitateSirues));
            idSheet.createRow(1).createCell(0).setCellValue(transferId);
            //set sheet very hidden
            template.setSheetHidden(template.getNumberOfSheets() - 1, Workbook.SHEET_STATE_VERY_HIDDEN);
            try (OutputStream out = response.getOutputStream()) {
                template.write(out);
                out.flush();
            }
        } catch (InvalidFormatException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } finally {
            //out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
