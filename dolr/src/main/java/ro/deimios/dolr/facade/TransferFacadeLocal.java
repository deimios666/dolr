/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.Transfer;

/**
 *
 * @author Deimios
 */
@Local
public interface TransferFacadeLocal {

    void create(Transfer transfer);

    void edit(Transfer transfer);

    void remove(Transfer transfer);

    Transfer find(Object id);

    List<Transfer> findAll();

    List<Transfer> findRange(int[] range);

    int count();

    public java.util.List<ro.deimios.dolr.entity.Transfer> findAllByRegistruByTip(int registruId, int tipTransferId);

    public java.util.List<ro.deimios.dolr.entity.Transfer> findAllForUnitate(int sirues, int tipTrandsferId);

    public java.util.List<ro.deimios.dolr.entity.Transfer> findResponses(int id);

    public void deleteByRegistruByTip(int registruId);
    
}
