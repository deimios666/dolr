/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.TipStat;

/**
 *
 * @author Deimios
 */
@Local
public interface TipStatFacadeLocal {

    void create(TipStat tipStat);

    void edit(TipStat tipStat);

    void remove(TipStat tipStat);

    TipStat find(Object id);

    List<TipStat> findAll();

    List<TipStat> findRange(int[] range);

    int count();
    
}
