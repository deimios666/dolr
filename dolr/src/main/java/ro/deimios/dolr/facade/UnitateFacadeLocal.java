/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.Unitate;

/**
 *
 * @author Deimios
 */
@Local
public interface UnitateFacadeLocal {

    void create(Unitate unitate);

    void edit(Unitate unitate);

    void remove(Unitate unitate);

    Unitate find(Object id);

    List<Unitate> findAll();

    List<Unitate> findRange(int[] range);

    int count();

    public java.util.List<ro.deimios.dolr.entity.Unitate> findAllNonISJPJ();
}
