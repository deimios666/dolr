/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.User;

/**
 *
 * @author Deimios
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    @Override
    public User findByUsername(String username) {
        Query query = em.createNamedQuery("User.findByUsername");
        query.setParameter("username", username);
        List<User> userList = query.getResultList();
        if (userList != null && userList.size() == 1) {
            return userList.get(0);
        } else {
            return null;
        }
    }

    @Override
    public int userCount() {
        Query query = em.createNamedQuery("User.countUsers");
        return ((Number) query.getSingleResult()).intValue();
    }
}
