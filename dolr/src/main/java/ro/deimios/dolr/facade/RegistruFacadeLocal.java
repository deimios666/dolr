/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.Registru;

/**
 *
 * @author Deimios
 */
@Local
public interface RegistruFacadeLocal {

    void create(Registru registru);

    void edit(Registru registru);

    void remove(Registru registru);

    Registru find(Object id);

    List<Registru> findAll();

    List<Registru> findRange(int[] range);

    int count();

    public void deleteByNr(int nr);
    
}
