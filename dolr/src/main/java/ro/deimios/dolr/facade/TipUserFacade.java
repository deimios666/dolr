/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.TipUser;

/**
 *
 * @author Deimios
 */
@Stateless
public class TipUserFacade extends AbstractFacade<TipUser> implements TipUserFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipUserFacade() {
        super(TipUser.class);
    }

    @Override
    public TipUser findByNume(String nume) {
        Query query = em.createNamedQuery("TipUser.findByName");
        query.setParameter("name", nume);
        return (TipUser) query.getSingleResult();
    }

}
