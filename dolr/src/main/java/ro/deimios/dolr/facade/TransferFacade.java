/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.Transfer;

/**
 *
 * @author Deimios
 */
@Stateless
public class TransferFacade extends AbstractFacade<Transfer> implements TransferFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TransferFacade() {
        super(Transfer.class);
    }

    @Override
    public List<Transfer> findAllByRegistruByTip(int registruId, int tipTransferId) {
        Query query = em.createNamedQuery("Transfer.findAllByRegistruByTip");
        query.setParameter("registru", registruId);
        query.setParameter("tiptransfer", tipTransferId);
        return query.getResultList();
    }
    
    @Override
    public void deleteByRegistruByTip(int registruId) {
        Query query = em.createNamedQuery("Transfer.deleteByRegistru");
        query.setParameter("registru", registruId);
        query.executeUpdate();
    }

    @Override
    public List<Transfer> findAllForUnitate(int sirues, int tipTrandsferId) {
        Query query = em.createNamedQuery("Transfer.findAllForUnitate");
        query.setParameter("sirues", sirues);
        query.setParameter("tiptransfer", tipTrandsferId);
        return query.getResultList();
    }
    
    @Override
    public List<Transfer> findResponses(int id){
        Query query = em.createNamedQuery("Transfer.findResponses");
        query.setParameter("id", id);
        return query.getResultList();
    }
    
}
