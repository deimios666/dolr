/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.TipTransfer;

/**
 *
 * @author Deimios
 */
@Local
public interface TipTransferFacadeLocal {

    void create(TipTransfer tipTransfer);

    void edit(TipTransfer tipTransfer);

    void remove(TipTransfer tipTransfer);

    TipTransfer find(Object id);

    List<TipTransfer> findAll();

    List<TipTransfer> findRange(int[] range);

    int count();
    
}
