/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dolr.entity.TipUnitate;

/**
 *
 * @author Deimios
 */
@Local
public interface TipUnitateFacadeLocal {

    void create(TipUnitate tipUnitate);

    void edit(TipUnitate tipUnitate);

    void remove(TipUnitate tipUnitate);

    TipUnitate find(Object id);

    List<TipUnitate> findAll();

    List<TipUnitate> findRange(int[] range);

    int count();

    public TipUnitate findByNume(String nume);
    
}
