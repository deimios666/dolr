/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.TipUnitate;

/**
 *
 * @author Deimios
 */
@Stateless
public class TipUnitateFacade extends AbstractFacade<TipUnitate> implements TipUnitateFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public TipUnitate findByNume(String nume) {
        Query query = em.createNamedQuery("TipUnitate.findByNume");
        query.setParameter("nume", nume);
        return (TipUnitate) query.getSingleResult();
    }

    public TipUnitateFacade() {
        super(TipUnitate.class);
    }

}
