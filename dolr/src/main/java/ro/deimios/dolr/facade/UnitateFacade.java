/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.Unitate;

/**
 *
 * @author Deimios
 */
@Stateless
public class UnitateFacade extends AbstractFacade<Unitate> implements UnitateFacadeLocal {

    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UnitateFacade() {
        super(Unitate.class);
    }

    @Override
    public List<Unitate> findAllNonISJPJ() {
        Query query = em.createNamedQuery("Unitate.findAllNonISJPJ");
        return query.getResultList();
    }
}
