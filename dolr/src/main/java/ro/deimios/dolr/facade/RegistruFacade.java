/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dolr.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dolr.entity.Registru;

/**
 *
 * @author Deimios
 */
@Stateless
public class RegistruFacade extends AbstractFacade<Registru> implements RegistruFacadeLocal {
    @PersistenceContext(unitName = "dolrPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RegistruFacade() {
        super(Registru.class);
    }
    
    @Override
    public void deleteByNr(int nr){
        Query query = em.createNamedQuery("Registru.deleteByNr");
        query.setParameter("nr", nr);
        query.executeUpdate();
    
    }
    
}
